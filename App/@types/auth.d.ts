interface AuthInfo {
  access_token: string;
  access_token_refresh: string;
}
interface UserInfo {
  default_address: string;
  expiredTime: number;
  fullName: string;
  source: string;
  status: number;
  token: string;
  userId: number;
  userName: string;
}
