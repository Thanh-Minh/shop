interface FilterNoti {
  data: {
    deviceId: string;
    type: number;
  };
  endTime?: number;
  page?: number;
  pageSize?: number;
  startTime?: number;
}
interface NotifyItem {
  id: string;
  title: string;
  message: string;
  image: string;
  userId: number;
  payloadData: {
    type: number;
    url: string;
    time: number;
    message: string;
    idNoti: string;
  };
  readed: number;
  time: number;
  readedTime: number;
  deleteTime: number;
  en_title: string;
  en_message: string;
}
