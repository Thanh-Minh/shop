interface OrderItem {
  acceptedTime: number;
  createdTime: number;
  deliveryAddress: string;
  deliveryNote: string;
  detailProdName: string;
  detailProductId: number;
  discount: number;
  id: number;
  orderCode: string;
  orderId: number;
  productName: string;
  productPrice: number;
  quantity: number;
  shopId: number;
  status: number;
  successTime: number;
  unitName: string;
  updatedTime: number;
  userId: number;
}
