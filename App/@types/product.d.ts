interface ProductItem {
  basePrice: number;
  categoryId: number;
  certImages?: string[];
  classId: number;
  companyDiscount: number;
  createdTime?: number;
  description: string;
  discount: number;
  expiredTime: number;
  groupId: number;
  images: string[];
  isOutOfStock?: true;
  listComboProduct: [
    {
      comboSize: number;
      id: number;
      images: string[];
      name: string;
      price: number;
      productId: number;
      quantity: number;
      type: number;
    },
  ];
  listDetailProdMongo: [
    {
      comboSize?: number;
      id?: number;
      images?: string[];
      name: string;
      price: number;
      productId?: number;
      quantity: number;
      type: number;
    },
  ];
  location?: {
    coordinates: number[];
    type: string;
  };
  name: string;
  note: string;
  productId: number;
  rate?: number;
  shopId?: number;
  shopName?: string;
  special: number;
  status?: number;
  type: number;
  unitId: number;
  unitName?: string;
  updatedTime?: number;
}

interface ProductChildItem {
  comboSize: number;
  discount: number;
  expiredTime: number;
  id: number;
  images: string[];
  name: string;
  price: number;
  productId: number;
  productName: string;
  quantity: number;
  type: number;
  unitName: string;
}
