interface OwnerDataRequest {
  cert_images: string;
  goods_type: number;
  name: string;
  phone: String;
  lat: number;
  lng: number;
  saleTime: {
    afternoons?: number[];
    mornings?: number[];
    nights?: number[];
    noons?: number[];
  };
}
