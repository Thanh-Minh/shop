interface UpdateOwnerInfo {
  address: string;
  birthday: number;
  cert_images: string[];
  full_name: string;
  gender: number;
  ward_id: number;
}
interface OwnerInfo {
  address: {
    buildingId: number;
    createdTime: number;
    districtId: number;
    id: number;
    latitude: number;
    longitude: number;
    name: string;
    updatedTime: number;
    userId: number;
    wardId: number;
  };
  birthday: number;
  fullName: string;
  gender: number;
  phone: string;
}
interface UserBank {
  accName: string;
  accNum: string;
  bankCode: string;
  bankName: string;
  createdTime: number;
  id: number;
  updatedTime: number;
  userId: number;
}
interface InfoShop {
  address: string;
  districtId: number;
  name: string;
  phone1: string;
  phone2: string;
  wardId: number;
}
