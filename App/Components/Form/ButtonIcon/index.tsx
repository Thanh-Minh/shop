import React, {ReactElement} from 'react';
import {
  GestureResponderEvent,
  Image,
  ImageSourcePropType,
  Text,
  TouchableOpacity,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import styles from './styles';

interface Props {
  title: string;
  color: (string | number)[];
  image: ImageSourcePropType;
  isProductItem?: boolean;
  action?: ((event: GestureResponderEvent) => void) | undefined;
}

const ButtonIcon = (props: Props): ReactElement => {
  return (
    <TouchableOpacity style={styles.containerSmall} onPress={props.action}>
      <LinearGradient
        colors={props.color}
        start={{x: 1.0, y: 0.5}}
        end={{x: 0.0, y: 0.5}}
        locations={[0.0, 1.0]}
        style={styles.gradient}>
        <Text style={styles.smallText}>{props.title}</Text>
        <Image source={props.image} style={styles.icon} />
      </LinearGradient>
    </TouchableOpacity>
  );
};
export default ButtonIcon;
