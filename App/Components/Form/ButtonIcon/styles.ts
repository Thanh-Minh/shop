import {Palette} from 'App/Theme/Palette';
import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default StyleSheet.create({
  containerSmall: {
    height: 30,
    marginTop: 5,
  },
  gradient: {
    flexDirection: 'row',
    height: 30,
    width: 90,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },
  smallText: {
    fontSize: RFValue(9, 580),
    fontWeight: '600',
    color: Palette.white,
  },
  icon: {
    height: 12,
    width: 12,
    resizeMode: 'contain',
    marginLeft: 5,
  },
});
