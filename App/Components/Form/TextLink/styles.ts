import {Palette} from 'App/Theme/Palette';
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  groupTextSignIn: {
    height: 30,
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 20,
  },
  textSignIn: {
    color: Palette.color_1f1,
  },
  marginLeft5: {
    marginLeft: 5,
  },
});
