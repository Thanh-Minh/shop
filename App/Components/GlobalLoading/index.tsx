import {deviceHeight, deviceWidth} from 'App/Config/ScreenConfig';
import {selectGlobalLoading} from 'App/Selector/GlobalSelector';
import {Palette} from 'App/Theme/Palette';
import React, {ReactElement} from 'react';
import {ActivityIndicator} from 'react-native';
import Modal from 'react-native-modal';
import {useSelector} from 'react-redux';

const GlobalLoading = (): ReactElement => {
  const isLoading = useSelector(selectGlobalLoading);
  return (
    <Modal
      isVisible={isLoading}
      deviceHeight={deviceHeight}
      deviceWidth={deviceWidth}>
      <ActivityIndicator animating={true} size="large" color={Palette.white} />
    </Modal>
  );
};
export default GlobalLoading;
