import {useNavigation} from '@react-navigation/native';
import media from 'App/assets/media';
import React, {ReactElement} from 'react';
import {Image, TouchableOpacity} from 'react-native';

const ImageButtonBack = (): ReactElement => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      onPress={() => {
        navigation.goBack();
      }}>
      <Image
        source={media.chevron_left}
        style={{
          width: 20,
          height: 20,
          marginLeft: 15,
          resizeMode: 'contain',
        }}
      />
    </TouchableOpacity>
  );
};
export default ImageButtonBack;
