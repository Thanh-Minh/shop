import media from 'App/assets/media';
import env from 'App/Config/Enviroment/env';
import {createFormData} from 'App/Lib/dataHelper';
import {Actions as DataAction} from 'App/Redux/DataRedux';
import React, {Dispatch, ReactElement, SetStateAction, useState} from 'react';
import {ActivityIndicator, TouchableOpacity, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import ImagePicker from 'react-native-image-crop-picker';
import {useDispatch} from 'react-redux';
import PopUpImage from '../PopUpImage';
import styles from './styles';

interface Props {
  index: number;
  listImage: string[];
  setListValue: Dispatch<SetStateAction<string[]>>;
}

const ImageHolder = (props: Props): ReactElement => {
  const dispatch = useDispatch();
  const [image, setImage] = useState('');
  const [loading, setLoading] = useState(false);
  const [chooseVisible, setChooseVisible] = useState(false);

  const uploadImage = (img: any) => {
    dispatch(
      DataAction.uploadImage(
        {formData: createFormData(img)},
        {
          onBeginning: () => {
            setLoading(true);
          },
          onSuccess: (response: any) => {
            setImage(response.data[0]);
            props.listImage[props.index] = response.data[0];
            props.setListValue([...props.listImage]);
          },
          onFailure: (error: any) => {
            console.warn(error);
          },
          onFinish: () => {
            setLoading(false);
          },
        },
      ),
    );
  };

  const open_gallery = () => {
    setChooseVisible(false);
    setTimeout(() => {
      ImagePicker.openPicker({
        width: 300,
        height: 400,
        cropping: true,
      })
        .then(img => {
          uploadImage(img);
        })
        .catch(err => {
          console.warn(err);
        });
    }, 200);
  };

  const open_camera = () => {
    setChooseVisible(false);
    setTimeout(() => {
      ImagePicker.openCamera({
        width: 300,
        height: 400,
        cropping: true,
      })
        .then(img => {
          uploadImage(img);
        })
        .catch(err => {
          console.warn(err);
        });
    }, 200);
  };

  return (
    <>
      <TouchableOpacity onPress={() => (image ? null : setChooseVisible(true))}>
        <View style={styles.containerImage}>
          {loading ? (
            <ActivityIndicator style={styles.loading} animating={true} />
          ) : null}
          <FastImage
            source={image ? {uri: env.API_ENDPOINT + image} : media.add}
            style={image ? styles.image : styles.iconAdd}
            onLoadStart={() => setLoading(true)}
            onLoadEnd={() => setLoading(false)}
          />
          {image ? (
            <TouchableOpacity
              hitSlop={{top: 30, bottom: 30, left: 30, right: 30}}
              onPress={() => setImage('')}>
              <FastImage source={media.delete} style={styles.iconDel} />
            </TouchableOpacity>
          ) : null}
        </View>
      </TouchableOpacity>
      <PopUpImage
        visible={chooseVisible}
        setVisible={setChooseVisible}
        openCamera={() => open_camera()}
        openGallery={() => open_gallery()}
      />
    </>
  );
};
export default ImageHolder;
