import {Palette} from 'App/Theme/Palette';
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  containerImage: {
    height: 70,
    width: 70,
    marginHorizontal: 10,
    borderWidth: 1,
    borderColor: Palette.color_c6c,
    borderStyle: 'dashed',
    borderRadius: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconAdd: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
  },
  iconDel: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
    position: 'absolute',
    right: 0,
    top: -30,
    bottom: 0,
    left: 15,
  },
  loading: {
    position: 'absolute',
    right: 0,
    top: 0,
    bottom: 0,
    left: 0,
  },
  image: {
    position: 'absolute',
    right: 0,
    top: 0,
    bottom: 0,
    left: 0,
    resizeMode: 'cover',
  },
});
