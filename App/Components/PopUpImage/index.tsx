import React, {Dispatch, SetStateAction} from 'react';
import {
  GestureResponderEvent,
  Modal,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import styles from './styles';

const PopUpImage = ({
  visible,
  setVisible,
  openCamera,
  openGallery,
}: {
  visible: boolean;
  setVisible: Dispatch<SetStateAction<boolean>>;
  openCamera: (event: GestureResponderEvent) => void;
  openGallery: (event: GestureResponderEvent) => void;
}) => {
  return (
    <Modal
      transparent={true}
      visible={visible}
      presentationStyle="overFullScreen">
      <View style={styles.overlay} />
      <View style={styles.centeredView}>
        <View style={styles.modalContent}>
          <TouchableOpacity
            style={styles.openButtonNoLine}
            onPress={openCamera}>
            <Text style={[styles.textStyle, styles.option]}>{'Camera'}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.openButton} onPress={openGallery}>
            <Text style={[styles.textStyle, styles.option]}>{'Gallery'}</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.modalCancel}>
          <TouchableOpacity
            style={styles.openButtonNoLine}
            onPress={() => {
              setVisible(false);
            }}>
            <Text style={[styles.textStyle, styles.cancel]}>{'Cancel'}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default PopUpImage;
