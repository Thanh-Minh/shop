import {ParamListBase} from '@react-navigation/native';

export enum ScreenMap {
  CheckLogin = 'CheckLogin',
  SignIn = 'SignIn',
  SignUp = 'SignUp',
  Otp = 'Otp',
  Provision = 'Provision',
  UpdateOwnerShopInfo = 'UpdateOwnerShopInfo',
  UpdateShopInfo = 'UpdateShopInfo',
  Home = 'Home',
  CreateProduct = 'CreateProduct',
  UpdateProduct = 'UpdateProduct',
  CreateCombo = 'CreateCombo',
  UpdateCombo = 'UpdateCombo',
  ListOrder = 'ListOrder',
  OrderDetail = 'OrderDetail',
  Notification = 'Notification',
  NotificationDetail = 'NotificationDetail',
  Statistic = 'Statistic',
  MainProfile = 'MainProfile',
  Profile = 'Profile',
  ShopProfile = 'ShopProfile',
  BankProfile = 'BankProfile',
}

export interface ScreenParams extends ParamListBase {
  [ScreenMap.SignIn]: {};
  [ScreenMap.SignUp]: {};
  [ScreenMap.Otp]: {};
  [ScreenMap.Provision]: {};
  [ScreenMap.UpdateOwnerShopInfo]: {};
  [ScreenMap.UpdateShopInfo]: {};
  [ScreenMap.Home]: {};
  [ScreenMap.CreateProduct]: {};
  [ScreenMap.UpdateProduct]: {productId: number};
  [ScreenMap.CreateCombo]: {};
  [ScreenMap.UpdateCombo]: {productId: number};
  [ScreenMap.ListOrder]: {};
  [ScreenMap.OrderDetail]: {item: OrderItem};
  [ScreenMap.Notification]: {};
  [ScreenMap.NotificationDetail]: {item: NotifyItem};
  [ScreenMap.Statistic]: {};
  [ScreenMap.MainProfile]: {};
  [ScreenMap.Profile]: {};
  [ScreenMap.ShopProfile]: {};
  [ScreenMap.BankProfile]: {};
}
