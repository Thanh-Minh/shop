import {Color} from 'App/Theme';
import {Dimensions} from 'react-native';

export const deviceWidth = Dimensions.get('window').width;
export const deviceHeight = Dimensions.get('window').height;

export const screenHeight = Math.round(deviceHeight);
export const screenWidth = Math.round(deviceWidth);

const ScreenConfig = {
  headerColor: Color.headerSafe,
  safeAreaColor: Color.headerSafe,
};
export default ScreenConfig;
