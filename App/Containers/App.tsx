import GlobalLoading from 'App/Components/GlobalLoading';
import store from 'App/store';
import React from 'react';
import codePush from 'react-native-code-push';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {enableScreens} from 'react-native-screens';
import {Provider} from 'react-redux';
import Navigation from '../navigation';

enableScreens();

const App = () => {
  return (
    <SafeAreaProvider>
      <Provider store={store}>
        <Navigation />
        <GlobalLoading />
      </Provider>
    </SafeAreaProvider>
  );
};
export default codePush(App);
