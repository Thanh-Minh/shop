import AsyncStorage from '@react-native-community/async-storage';
import {BaseScreenProps} from 'App/@types/screen-type';
import {ScreenMap} from 'App/Config/NavigationConfig';
import {code} from 'App/Lib/codeHelpers';
import {updateAccessTokenToHeader} from 'App/Lib/fetchHelpers';
import {Actions as AuthActions} from 'App/Redux/AuthRedux';
import {Actions as GlobalAction} from 'App/Redux/GlobalRedux';
import React, {ReactElement, useCallback, useEffect} from 'react';
import {ActivityIndicator, Alert} from 'react-native';
import firebase from 'react-native-firebase';
import {useDispatch} from 'react-redux';

const CheckLoginScreen = (
  props: BaseScreenProps<ScreenMap.CheckLogin>,
): ReactElement => {
  const dispatch = useDispatch();

  useEffect(() => {
    checkPermission();
    createNotificationListeners();
    handleNotification();
    checkLogin();
  }, []);

  const getTokenLogin = async () => {
    let newfcmToken = await firebase.messaging().getToken();
    if (newfcmToken) {
      await AsyncStorage.setItem('fcmToken', newfcmToken);
      insertDevice(newfcmToken);
    } else {
      insertDevice(newfcmToken);
    }
  };

  const checkLogin = () => {
    AsyncStorage.getItem('access_token').then(userToken => {
      if (userToken) {
        updateAccessTokenToHeader({
          accessToken: userToken,
        });
        getTokenLogin();
        props.navigation.navigate('BottomTab', {
          screen: 'Home',
        });
      } else {
        props.navigation.navigate(ScreenMap.SignIn, {});
      }
    });
  };

  const checkPermission = async () => {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      getToken();
    } else {
      requestPermission();
    }
  };
  const getToken = async () => {
    let fcmToken = await firebase.messaging().getToken();
    // console.log(fcmToken);
    if (fcmToken) {
      await AsyncStorage.setItem('fcmToken', fcmToken);
    } else {
      requestPermission();
    }
  };

  const requestPermission = async () => {
    try {
      await firebase.messaging().requestPermission();
      getToken();
    } catch (error) {}
  };

  const handleNotification = async () => {
    firebase.notifications().onNotificationOpened(async notificationOpen => {
      const token = await AsyncStorage.getItem('access_token');
      const notification = notificationOpen.notification;
      let fcmToken = await AsyncStorage.getItem('fcmToken');
      if (token) {
        if (notification.data.type == '0') {
          const arr: [] = [];
          // updateStatusNotify(fcmToken, arr, 1, 3, 1);
          // navigation.navigate('BottomTab', {screen: 'Notify'});
          firebase
            .notifications()
            .removeDeliveredNotification(notification.notificationId);
          firebase.notifications().removeAllDeliveredNotifications();
        } else {
          const arr = [];
          arr.push(notification.data.idNoti);
          // updateStatusNotify(fcmToken, arr, 1, 2, 2);
          setTimeout(() => {
            // navigation.navigate('BottomTab', {screen: 'Notify'});
            firebase
              .notifications()
              .removeDeliveredNotification(notification.notificationId);
            firebase.notifications().removeAllDeliveredNotifications();
          }, 200);
        }
      }
    });
    await firebase
      .notifications()
      .getInitialNotification()
      .then(notificationOpen => {
        if (notificationOpen) {
          const notification = notificationOpen.notification;
          setTimeout(() => {
            if (notification.data) {
              Alert.alert('Thông báo', notification.data.message);
            } else {
              Alert.alert('Thông báo', notification.body);
            }
          }, 1000);
          firebase
            .notifications()
            .removeDeliveredNotification(notification.notificationId);
        }
      });
  };

  const createNotificationListeners = async () => {
    firebase.messaging().onTokenRefresh(async () => {
      getTokenLogin();
    });
    firebase.notifications().onNotification(notification => {
      firebase.notifications().displayNotification(notification);
      // setTimeout(() => {
      // if (token) {
      // getDataNotification();
      // }
      // }, 50);
    });
    firebase.messaging().onMessage(message => {
      console.log(message);
      // setTimeout(() => {
      // if (token) {
      // getDataNotification();
      // }
      // }, 50);
    });
  };
  const insertDevice = useCallback((newfcmToken: string) => {
    dispatch(
      AuthActions.insertDeviceFireBase(
        {
          deviceId: newfcmToken,
        },
        {
          onBeginning: () => {
            dispatch(GlobalAction.setShowLoading({isLoading: true}));
          },
          onSuccess: (response: any) => {
            switch (response.errorCode) {
              case code.success:
                console.log('Insert deviceId Success');
                break;
            }
          },
          onFailure: (error: any) => {
            console.warn('error', error);
          },
          onFinish: () => {
            dispatch(GlobalAction.setShowLoading({isLoading: false}));
          },
        },
      ),
    );
  }, []);

  return <ActivityIndicator />;
};
export default CheckLoginScreen;
