import {BaseScreenProps} from 'App/@types/screen-type';
import media from 'App/assets/media';
import ButtonSubmit from 'App/Components/Form/ButtonSubmit';
import {ScreenMap} from 'App/Config/NavigationConfig';
import {code} from 'App/Lib/codeHelpers';
import {Actions as AuthActions} from 'App/Redux/AuthRedux';
import {Actions as GlobalAction} from 'App/Redux/GlobalRedux';
import {Palette} from 'App/Theme/Palette';
import React, {ReactElement, useState} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useDispatch} from 'react-redux';
import styles from './styles';

const ProvisionScreen = (
  props: BaseScreenProps<ScreenMap.Provision>,
): ReactElement => {
  const dispatch = useDispatch();
  const [isConfirm, setIsConfirm] = useState(false);

  const accept_tos = () => {
    if (isConfirm) {
      dispatch(
        AuthActions.acceptTermOfService(
          {},
          {
            onBeginning: () => {
              dispatch(GlobalAction.setShowLoading({isLoading: true}));
            },
            onSuccess: (response: any) => {
              switch (response.errorCode) {
                case code.success:
                  props.navigation.navigate(ScreenMap.UpdateOwnerShopInfo, {});
                  break;
                case code.unauthorize:
                  props.navigation.navigate(ScreenMap.SignIn, {});
                  break;
              }
            },
            onFailure: (error: any) => {
              console.warn('error', error);
            },
            onFinish: () => {
              dispatch(GlobalAction.setShowLoading({isLoading: false}));
            },
          },
        ),
      );
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.scroll}>
        <View style={styles.groupContent}>
          <Text style={styles.title}>{'Bán hàng chuyên nghiệp'}</Text>
          <Text style={styles.subTitle}>
            {'Quản lý cửa hàng của bạn một cách hiệu quả hơn'}
          </Text>
          <Image source={media.provision} style={styles.image} />
          <Text style={styles.thank}>
            {'Cảm ơn đối tác đã tin tưởng và lựa chọn đồng hành cùng chúng tôi \n' +
              'Vui lòng hoàn tất biểu mẫu và cung cấp đầy đủ nội dung theo hướng dẫn để bắt đầu bán hàng nhanh nhất.'}
          </Text>
          <Text style={styles.provisonTitle}>
            {
              'Để tiếp tục bước đăng ký, nhà bán hàng đảm bảo cung cấp chứng nhận hoàng hóa chính hàng và được phân phối hàng tại Việt Nam'
            }
          </Text>
          <Text style={styles.provisonSection}>
            {'A. Đối với hàng hóa sản xuất trong nước:'}
          </Text>
          <Text style={styles.provisonSectionContent}>
            {'1. Giấy chứng nhận an toàn thực phẩm do cơ quan nhà nước chứng nhận.' +
              '\n2. Giấy phép phân phối hàng hóa, hoặc hợp đồng mua bán hàng hóa đối với hoàng hóa do doanh nghiệp cung cấp.' +
              '\n3. Công bố tiêu chuẩn, quy chuẩn rõ ràng, chất phụ gia, chất bảo quản trong thực phẩm.'}
          </Text>
          <Text style={styles.provisonSection}>
            {'B. Đối với hoàng hóa nhập khẩu:'}
          </Text>
          <Text style={styles.provisonSectionContent}>
            {'1. Giấy chứng nhận đại lý phân phối hoặc hợp đồng mua bán hàng hóa đối với hàng hóa do doanh nghiệp phân phối hoặc mua đi bán lại.' +
              '\n2. Giấy tờ thông quan có dấu thông quan của cửa khẩu đối với hàng hóa nhập khẩu.'}
          </Text>
          <View style={styles.groupConfirm}>
            <TouchableOpacity onPress={() => setIsConfirm(!isConfirm)}>
              <Image
                source={isConfirm ? media.checked : media.unchecked}
                style={styles.iconCheck}
              />
            </TouchableOpacity>
            <Text style={styles.textConfirm}>
              {
                'Tôi đã đọc và hoàn toàn đồng ý với các điều khoản, chính sách & quy định khi kinh doanh trên sàn'
              }
            </Text>
          </View>
        </View>
        <ButtonSubmit
          title="Hoàn tất biểu mẫu và gửi"
          color={
            isConfirm
              ? [Palette.color_ff0, Palette.color_ff4]
              : [Palette.color_ccc, Palette.color_ccc]
          }
          action={() => (isConfirm ? accept_tos() : null)}
        />
      </ScrollView>
    </SafeAreaView>
  );
};
export default ProvisionScreen;
