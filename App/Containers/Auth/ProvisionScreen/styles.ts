import {screenHeight} from 'App/Config/ScreenConfig';
import {Palette} from 'App/Theme/Palette';
import {Platform, StyleSheet} from 'react-native';
import {getBottomSpace} from 'react-native-iphone-x-helper';
import {RFValue} from 'react-native-responsive-fontsize';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Palette.white,
    marginBottom: getBottomSpace(),
  },
  scroll: {
    marginTop: Platform.OS === 'android' ? 20 : 0,
    marginBottom: Platform.OS === 'android' ? 20 : 0,
  },
  groupContent: {
    flex: 1,
    alignItems: 'center',
    marginHorizontal: 15,
  },
  title: {
    color: Palette.color_ff3,
    fontSize: RFValue(20, 580),
    fontWeight: 'bold',
    lineHeight: 26,
  },
  subTitle: {
    marginVertical: 10,
    fontSize: RFValue(10, 580),
    color: Palette.color_1f1,
    lineHeight: 14,
  },
  image: {
    height: screenHeight / 4.5,
    resizeMode: 'contain',
  },
  thank: {
    marginTop: 10,
    fontSize: RFValue(8.5, 580),
    color: Palette.color_1f1,
    lineHeight: 14,
    textAlign: 'center',
  },
  provisonTitle: {
    marginTop: 20,
    fontSize: RFValue(8.5, 580),
    color: Palette.color_1f1,
    lineHeight: 16,
    alignSelf: 'flex-start',
  },
  provisonSection: {
    fontSize: RFValue(8.5, 580),
    fontWeight: 'bold',
    color: Palette.color_1f1,
    lineHeight: 16,
    alignSelf: 'flex-start',
  },
  provisonSectionContent: {
    fontSize: RFValue(8.5, 580),
    color: Palette.color_1f1,
    lineHeight: 16,
    alignSelf: 'flex-start',
  },
  groupConfirm: {
    height: 50,
    flexDirection: 'row',
    marginHorizontal: 10,
    marginTop: 20,
  },
  iconCheck: {
    width: 18,
    height: 18,
  },
  textConfirm: {
    fontSize: RFValue(9.5, 580),
    marginLeft: 10,
  },
});
