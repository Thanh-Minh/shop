import AsyncStorage from '@react-native-community/async-storage';
import {BaseScreenProps} from 'App/@types/screen-type';
import {ScreenMap} from 'App/Config/NavigationConfig';
import {code, user_code} from 'App/Lib/codeHelpers';
import {updateAccessTokenToHeader} from 'App/Lib/fetchHelpers';
import {validate_password, validate_phone} from 'App/Lib/validateHelpers';
import {Actions as AuthActions} from 'App/Redux/AuthRedux';
import {Actions as GlobalAction} from 'App/Redux/GlobalRedux';
import {Palette} from 'App/Theme/Palette';
import React, {ReactElement, useCallback, useEffect, useState} from 'react';
import {Text, View} from 'react-native';
import firebase from 'react-native-firebase';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useDispatch} from 'react-redux';
import media from '../../../assets/media';
import ButtonSubmit from '../../../Components/Form/ButtonSubmit';
import Input from '../../../Components/Form/Input';
import styles from './styles';

const SignInScreen = (
  props: BaseScreenProps<ScreenMap.SignIn>,
): ReactElement => {
  const dispatch = useDispatch();
  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');
  const [validatePhone, setValidatePhone] = useState('');
  const [validatePassword, setValidatePassword] = useState('');

  useEffect(() => {
    const unsubcribe = props.navigation.addListener('focus', () => {
      setPhone('');
      setPassword('');
      setValidatePhone('');
      setValidatePassword('');
    });
    return unsubcribe;
  }, [props.navigation]);

  const getTokenLogin = async () => {
    let newfcmToken = await firebase.messaging().getToken();
    if (newfcmToken) {
      await AsyncStorage.setItem('fcmToken', newfcmToken);
      insertDevice(newfcmToken);
    } else {
      insertDevice(newfcmToken);
    }
  };
  const insertDevice = useCallback((newfcmToken: string) => {
    dispatch(
      AuthActions.insertDeviceFireBase(
        {
          deviceId: newfcmToken,
        },
        {
          onBeginning: () => {
            dispatch(GlobalAction.setShowLoading({isLoading: true}));
          },
          onSuccess: (response: any) => {
            switch (response.errorCode) {
              case code.success:
                console.log('Insert deviceId Success');
                props.navigation.navigate('BottomTab', {
                  screen: 'Home',
                });
                break;
            }
          },
          onFailure: (error: any) => {
            console.warn('error', error);
          },
          onFinish: () => {
            dispatch(GlobalAction.setShowLoading({isLoading: false}));
          },
        },
      ),
    );
  }, []);

  const validate_data = (phone: string, password: string) => {
    let validate_fail = 0;
    if (phone.length <= 0) {
      setValidatePhone('Vui lòng nhập số điện thoại');
      validate_fail += 1;
    } else if (!validate_phone(phone)) {
      setValidatePhone('Vui lòng nhập số điện thoại từ 9 đến 11 số');
      validate_fail += 1;
    } else {
      setValidatePhone('');
    }
    if (password.length <= 0) {
      setValidatePassword('Vui lòng nhập mật khẩu');
      validate_fail += 1;
    } else if (!validate_password(password)) {
      setValidatePassword(
        'Vui lòng nhập mật khẩu từ 6 đến 12 ký tự, gồm số hoặc chữ',
      );
      validate_fail += 1;
    } else {
      setValidatePassword('');
    }
    return validate_fail == 0;
  };

  const login = useCallback(
    (phone: string, password: string) => {
      if (validate_data(phone, password)) {
        dispatch(
          AuthActions.login(
            {
              phone: phone,
              password: password,
            },
            {
              onBeginning: () => {
                dispatch(GlobalAction.setShowLoading({isLoading: true}));
              },
              onSuccess: (response: any) => {
                let direct = [
                  ScreenMap.Otp,
                  ScreenMap.Provision,
                  ScreenMap.UpdateShopInfo,
                  ScreenMap.UpdateOwnerShopInfo,
                ];
                switch (response.errorCode) {
                  case code.success:
                    switch (response.data.user.status) {
                      case user_code.block:
                        setValidatePassword('Tài khoản của bạn đã bị khoá');
                        break;
                      case user_code.updated_shop:
                        updateAccessTokenToHeader({
                          accessToken: response.data.access_token,
                        });
                        AsyncStorage.setItem(
                          'access_token',
                          response.data.access_token,
                        );
                        getTokenLogin();
                        break;
                      case user_code.register_done:
                        updateAccessTokenToHeader({
                          accessToken: response.data.access_token,
                        });
                        AsyncStorage.setItem(
                          'access_token',
                          response.data.access_token,
                        );
                        getTokenLogin();
                        break;
                      case user_code.shop_approve:
                        updateAccessTokenToHeader({
                          accessToken: response.data.access_token,
                        });
                        AsyncStorage.setItem(
                          'access_token',
                          response.data.access_token,
                        );
                        getTokenLogin();
                        break;
                      default:
                        updateAccessTokenToHeader({
                          accessToken: response.data.access_token,
                        });
                        AsyncStorage.setItem(
                          'access_token',
                          response.data.access_token,
                        );
                        props.navigation.navigate(
                          direct[response.data.user.status],
                          {},
                        );
                        break;
                    }
                    break;
                  default:
                    setValidatePassword(response.message);
                    break;
                }
              },
              onFailure: (error: any) => {
                console.warn('error', error);
              },
              onFinish: () => {
                dispatch(GlobalAction.setShowLoading({isLoading: false}));
              },
            },
          ),
        );
      }
    },
    [dispatch],
  );

  return (
    <SafeAreaView style={styles.container}>
      <KeyboardAwareScrollView style={{height: 300}}>
        <View style={styles.groupContent}>
          <Input
            label="Số điện thoại"
            image={media.phone}
            maxLength={11}
            keyboardNumber={true}
            placeHolder="Nhập số điện thoại của bạn"
            placeHolderColor={Palette.color_ccc}
            value={phone}
            setValue={setPhone}
            removeValidateText={setValidatePhone}
          />
          {validatePhone.length > 0 ? (
            <Text style={styles.validate}>{validatePhone}</Text>
          ) : null}
          <Input
            label="Mật khẩu"
            image={media.lock}
            maxLength={12}
            placeHolder="*******"
            placeHolderColor={Palette.color_ccc}
            isPassword={true}
            value={password}
            setValue={setPassword}
            removeValidateText={setValidatePassword}
          />
          {validatePassword.length > 0 ? (
            <Text style={styles.validate}>{validatePassword}</Text>
          ) : null}
          <View style={styles.groupSubmit}>
            <ButtonSubmit
              title="Đăng Nhập"
              color={[Palette.color_ff0, Palette.color_ff4]}
              action={() => login(phone, password)}
            />
            <ButtonSubmit
              title="Đăng Ký"
              color={[Palette.color_f48, Palette.color_f48]}
              action={() => props.navigation.navigate(ScreenMap.SignUp, {})}
            />
            <Text style={styles.version}>{'Version 0.1.1'}</Text>
          </View>
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};
export default SignInScreen;
