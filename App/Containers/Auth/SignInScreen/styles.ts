import {screenHeight} from 'App/Config/ScreenConfig';
import {Palette} from 'App/Theme/Palette';
import {StyleSheet} from 'react-native';
import {getBottomSpace} from 'react-native-iphone-x-helper';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Palette.white,
    marginBottom: getBottomSpace(),
  },
  groupContent: {
    flex: 1,
    marginTop: screenHeight / 4.5,
    marginBottom: 40,
  },
  groupSubmit: {
    marginTop: 10,
  },
  groupTextBelow: {
    height: 30,
    marginTop: 15,
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 30,
  },
  hairline: {
    flex: 1,
    height: 0.5,
    backgroundColor: Palette.color_ccc,
  },
  loginButtonBelowText1: {
    width: 130,
    textAlign: 'center',
    color: Palette.color_ccc,
  },
  validate: {
    color: 'red',
    marginTop: 9,
    marginHorizontal: 20,
  },
  version: {
    alignSelf: 'center',
    marginTop: 10,
    color: Palette.color_ccc,
  },
});
