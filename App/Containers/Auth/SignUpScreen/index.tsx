import AsyncStorage from '@react-native-community/async-storage';
import {BaseScreenProps} from 'App/@types/screen-type';
import {ScreenMap} from 'App/Config/NavigationConfig';
import {code} from 'App/Lib/codeHelpers';
import {updateAccessTokenToHeader} from 'App/Lib/fetchHelpers';
import {validate_password, validate_phone} from 'App/Lib/validateHelpers';
import {Actions as AuthActions} from 'App/Redux/AuthRedux';
import {Actions as GlobalAction} from 'App/Redux/GlobalRedux';
import {Palette} from 'App/Theme/Palette';
import React, {ReactElement, useCallback, useEffect, useState} from 'react';
import {Text, View} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {RFValue} from 'react-native-responsive-fontsize';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useDispatch} from 'react-redux';
import media from '../../../assets/media';
import ButtonSubmit from '../../../Components/Form/ButtonSubmit';
import Input from '../../../Components/Form/Input';
import TextLink from '../../../Components/Form/TextLink';
import styles from './styles';

const SignUpScreen = (
  props: BaseScreenProps<ScreenMap.SignUp>,
): ReactElement => {
  const dispatch = useDispatch();
  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const [validatePhone, setValidatePhone] = useState('');
  const [validatePassword, setValidatePassword] = useState('');
  const [validateConfirmPassword, setValidateConfirmPassword] = useState('');

  useEffect(() => {
    const unsubcribe = props.navigation.addListener('focus', () => {
      setPhone('');
      setPassword('');
      setConfirmPassword('');
      setValidatePhone('');
      setValidatePassword('');
      setValidateConfirmPassword('');
    });
    return unsubcribe;
  }, [props.navigation]);

  const validate_data = (
    phone: string,
    password: string,
    confirmPassword: string,
  ) => {
    let validate_fail = 0;
    if (phone.length <= 0) {
      setValidatePhone('Vui lòng nhập số điện thoại');
      validate_fail += 1;
    } else if (!validate_phone(phone)) {
      setValidatePhone('Vui lòng nhập số điện thoại từ 9 đến 11 số');
      validate_fail += 1;
    } else {
      setValidatePhone('');
    }
    if (password.length <= 0) {
      setValidatePassword('Vui lòng nhập mật khẩu');
      validate_fail += 1;
    } else if (!validate_password(password)) {
      setValidatePassword(
        'Vui lòng nhập mật khẩu từ 6 đến 12 ký tự, gồm số hoặc chữ',
      );
      validate_fail += 1;
    } else {
      setValidatePassword('');
    }
    if (confirmPassword != password) {
      setValidateConfirmPassword('Mật khẩu xác nhận không đúng');
      validate_fail += 1;
    } else {
      setValidateConfirmPassword('');
    }
    return validate_fail == 0;
  };

  const sendRegister = useCallback(
    (phone: string, password: string, confirmPassword: string) => {
      if (validate_data(phone, password, confirmPassword)) {
        dispatch(
          AuthActions.sendRegisterData(
            {
              phone: phone,
              password: confirmPassword,
            },
            {
              onBeginning: () => {
                dispatch(GlobalAction.setShowLoading({isLoading: true}));
              },
              onSuccess: (response: any) => {
                switch (response.errorCode) {
                  case code.success:
                    updateAccessTokenToHeader({
                      accessToken: response.data.access_token,
                    });
                    AsyncStorage.setItem(
                      'access_token',
                      response.data.access_token,
                    );
                    props.navigation.navigate(ScreenMap.Otp, {});
                    break;
                  case code.phone_exist:
                    setValidatePhone(response.message);
                    break;
                  case code.phone_invalid:
                    setValidatePhone(response.message);
                    break;
                }
              },
              onFailure: (error: any) => {
                console.warn('error', error);
              },
              onFinish: () => {
                dispatch(GlobalAction.setShowLoading({isLoading: false}));
              },
            },
          ),
        );
      }
    },
    [dispatch],
  );

  return (
    <SafeAreaView style={styles.container}>
      <KeyboardAwareScrollView>
        <View style={styles.form}>
          <View style={styles.groupInput}>
            <Input
              label="Số điện thoại"
              image={media.phone}
              maxLength={11}
              keyboardNumber={true}
              placeHolder="Nhập số điện thoại của bạn"
              placeHolderColor={Palette.color_ccc}
              value={phone}
              setValue={setPhone}
              removeValidateText={setValidatePhone}
            />
            {validatePhone.length > 0 ? (
              <Text style={styles.validate}>{validatePhone}</Text>
            ) : null}
            <Input
              label="Mật khẩu"
              image={media.lock}
              maxLength={12}
              placeHolder="*******"
              placeHolderColor={Palette.color_ccc}
              isPassword={true}
              value={password}
              setValue={setPassword}
              removeValidateText={setValidatePassword}
            />
            {validatePassword.length > 0 ? (
              <Text style={styles.validate}>{validatePassword}</Text>
            ) : null}
            <Input
              label="Nhập lại mật khẩu"
              image={media.lock}
              maxLength={12}
              placeHolder="*******"
              placeHolderColor={Palette.color_ccc}
              isPassword={true}
              value={confirmPassword}
              setValue={setConfirmPassword}
              removeValidateText={setValidateConfirmPassword}
            />
            {validateConfirmPassword.length > 0 ? (
              <Text style={styles.validate}>{validateConfirmPassword}</Text>
            ) : null}
          </View>
          <ButtonSubmit
            title="Đăng Ký"
            color={[Palette.color_ff0, Palette.color_ff4]}
            action={() => sendRegister(phone, password, confirmPassword)}
          />
          <TextLink
            textTitle="Bạn đã có tài khoản?"
            textLink="Đăng Nhập"
            fontSize={RFValue(12, 580)}
            color={Palette.color_e40}
            action={() => props.navigation.navigate(ScreenMap.SignIn, {})}
          />
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};
export default SignUpScreen;
