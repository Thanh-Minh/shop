import {BaseScreenProps} from 'App/@types/screen-type';
import media from 'App/assets/media';
import ButtonSubmit from 'App/Components/Form/ButtonSubmit';
import Input from 'App/Components/Form/Input';
import ImageHolder from 'App/Components/ImageHolder';
import {ScreenMap} from 'App/Config/NavigationConfig';
import {code} from 'App/Lib/codeHelpers';
import {convert_dropdown_data} from 'App/Lib/convertDataHelper';
import {updateAccessTokenToHeader} from 'App/Lib/fetchHelpers';
import {validate_name, validate_phone} from 'App/Lib/validateHelpers';
import {Actions as AuthActions} from 'App/Redux/AuthRedux';
import {Actions as DataAction} from 'App/Redux/DataRedux';
import {Actions as GlobalAction} from 'App/Redux/GlobalRedux';
import {selectUserData} from 'App/Selector/AuthSelector';
import {Palette} from 'App/Theme/Palette';
import moment from 'moment';
import React, {ReactElement, useCallback, useEffect, useState} from 'react';
import {Image, SafeAreaView, Text, TouchableOpacity, View} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {RFValue} from 'react-native-responsive-fontsize';
import {useDispatch, useSelector} from 'react-redux';
import styles from './styles';

const UpdateShopInfoScreen = (
  props: BaseScreenProps<ScreenMap.UpdateShopInfo>,
): ReactElement => {
  const dispatch = useDispatch();
  const dataUser = useSelector(selectUserData);
  const [shopName, setShopName] = useState('');
  const [phone, setPhone] = useState('');
  const [listImage, setListImage] = useState(['', '', '', '']);
  const [categories, setCategories] = useState(-1);
  const [listCategories, setListCategories] = useState([
    {
      label: 'Dữ liệu hiện tại chưa có',
      value: -1,
    },
  ]);
  const time_type = {
    morning: 0,
    noon: 1,
    afternoon: 2,
    night: 3,
  };

  const [showDatePicker, setShowDatePicker] = useState(false);
  const [chooseTimeType, setChooseTimeType] = useState(0);
  const [chooseStart, setChooseStart] = useState(false);
  const [timeEdit, setTimeEdit] = useState({start: '', end: ''});
  const [chooseTime, setChooseTime] = useState({
    morning: {
      ischeck: false,
      time: {start: '5:00', end: '10:00'},
    },
    noon: {
      ischeck: false,
      time: {start: '10:01', end: '14:00'},
    },
    afternoon: {
      ischeck: false,
      time: {start: '14:01', end: '19:00'},
    },
    night: {
      ischeck: false,
      time: {start: '19:01', end: '23:00'},
    },
  });

  const [validateShopName, setValidateShopName] = useState('');
  const [validatePhone, setValidatePhone] = useState('');
  const [validateCategories, setValidateCategories] = useState('');
  const [validateListImage, setValidateListImage] = useState('');
  const [validateChooseTime, setValidateChooseTime] = useState('');

  useEffect(() => {
    const unsubcribe = props.navigation.addListener('focus', () => {
      dispatch(
        DataAction.getCategories(
          {},
          {
            onBeginning: () => {},
            onSuccess: (response: any) => {
              setListCategories(convert_dropdown_data(response.data));
            },
            onFailure: () => {},
            onFinish: () => {},
          },
        ),
      );
    });
    return unsubcribe;
  }, [dispatch, props.navigation]);

  const handle_check = (type: number) => {
    let time_obj = {...chooseTime};
    switch (type) {
      case time_type.morning:
        time_obj.morning = {
          ...time_obj.morning,
          ischeck: !time_obj.morning.ischeck,
        };
        break;
      case time_type.noon:
        time_obj.noon = {...time_obj.noon, ischeck: !time_obj.noon.ischeck};
        break;
      case time_type.afternoon:
        time_obj.afternoon = {
          ...time_obj.afternoon,
          ischeck: !time_obj.afternoon.ischeck,
        };
        break;
      case time_type.night:
        time_obj.night = {
          ...time_obj.night,
          ischeck: !time_obj.night.ischeck,
        };
        break;
    }
    setChooseTime(time_obj);
  };

  const handle_choose_time = (time: string, type: number, isStart: boolean) => {
    let time_obj = {...chooseTime};
    let time_choose: {start: string; end: string};
    switch (type) {
      case time_type.morning:
        if (isStart) {
          time_choose = {...chooseTime.morning.time, start: time};
        } else {
          time_choose = {...chooseTime.morning.time, end: time};
        }
        time_obj.morning = {
          ...chooseTime.morning,
          time: time_choose,
        };
        setTimeEdit(time_obj.morning.time);
        break;
      case time_type.noon:
        if (isStart) {
          time_choose = {...chooseTime.noon.time, start: time};
        } else {
          time_choose = {...chooseTime.noon.time, end: time};
        }
        time_obj.noon = {...chooseTime.noon, time: time_choose};
        setTimeEdit(time_obj.noon.time);
        break;
      case time_type.afternoon:
        if (isStart) {
          time_choose = {...chooseTime.afternoon.time, start: time};
        } else {
          time_choose = {...chooseTime.afternoon.time, end: time};
        }
        time_obj.afternoon = {
          ...chooseTime.afternoon,
          time: time_choose,
        };
        setTimeEdit(time_obj.afternoon.time);
        break;
      case time_type.night:
        if (isStart) {
          time_choose = {...chooseTime.night.time, start: time};
        } else {
          time_choose = {...chooseTime.night.time, end: time};
        }
        time_obj.night = {
          ...chooseTime.night,
          time: time_choose,
        };
        setTimeEdit(time_obj.night.time);
        break;
    }
    setChooseTime(time_obj);
  };

  // const toLong = (time: string) => {
  //   const time_arr = time.split(':');
  //   const seconds =
  //     parseInt(time_arr[0], 10) * 60 * 60 + parseInt(time_arr[1], 10) * 60;
  //   return seconds * 1000;
  // };

  const toDouble = (time: string) => {
    const time_arr = time.split(':');
    return +time_arr[0] + +time_arr[1] / 60;
  };

  const convertTime = (time: {start: string; end: string}) => {
    return [toDouble(time.start), toDouble(time.end)];
  };

  const validate_data = (
    shopName: string,
    phone: string,
    categories: number,
    listImage: string[],
    chooseTime: {
      morning: {
        ischeck: boolean;
        time: {
          start: string;
          end: string;
        };
      };
      noon: {
        ischeck: boolean;
        time: {
          start: string;
          end: string;
        };
      };
      afternoon: {
        ischeck: boolean;
        time: {
          start: string;
          end: string;
        };
      };
      night: {
        ischeck: boolean;
        time: {
          start: string;
          end: string;
        };
      };
    },
  ) => {
    let validate_fail = 0;
    if (shopName.length <= 0) {
      setValidateShopName('Vui lòng nhập Tên cửa hàng');
      validate_fail += 1;
    } else if (!validate_name(shopName)) {
      setValidateShopName(
        'Vui lòng nhập Tên cửa hàng không bao gồm ký tự đặc biệt',
      );
      validate_fail += 1;
    }
    if (phone.length <= 0) {
      setValidatePhone('Vui lòng nhập số điện thoại');
      validate_fail += 1;
    } else if (!validate_phone(phone)) {
      setValidatePhone('Vui lòng nhập số điện thoại từ 9 đến 11 số');
      validate_fail += 1;
    }
    if (categories < 0) {
      setValidateCategories('Vui lòng chọn mặt hàng kinh doanh');
    }
    if (listImage.filter(Boolean).length < 4) {
      setValidateListImage(
        'Vui lòng thêm đủ ảnh chứng chỉ, giấy phép, giấy chứng nhận ATTP',
      );
      validate_fail += 1;
    }
    if (
      !chooseTime.morning.ischeck &&
      !chooseTime.noon.ischeck &&
      !chooseTime.afternoon.ischeck &&
      !chooseTime.night.ischeck
    ) {
      setValidateChooseTime('Vui lòng chọn thông tin giờ mở bán');
      validate_fail += 1;
    }
    return validate_fail == 0;
  };

  const updateShopInfo = useCallback(
    (
      shopName: string,
      phone: string,
      categories: number,
      listImage: string[],
      chooseTime: {
        morning: {
          ischeck: boolean;
          time: {
            start: string;
            end: string;
          };
        };
        noon: {
          ischeck: boolean;
          time: {
            start: string;
            end: string;
          };
        };
        afternoon: {
          ischeck: boolean;
          time: {
            start: string;
            end: string;
          };
        };
        night: {
          ischeck: boolean;
          time: {
            start: string;
            end: string;
          };
        };
      },
    ) => {
      if (validate_data(shopName, phone, categories, listImage, chooseTime)) {
        dispatch(
          AuthActions.updateShopInfo(
            {
              shopName: shopName,
              phone: phone,
              categories: categories,
              listImage: listImage,
              afternoon: chooseTime.afternoon.ischeck
                ? convertTime(chooseTime.afternoon.time)
                : null,
              morning: chooseTime.morning.ischeck
                ? convertTime(chooseTime.morning.time)
                : null,
              noon: chooseTime.noon.ischeck
                ? convertTime(chooseTime.noon.time)
                : null,
              night: chooseTime.night.ischeck
                ? convertTime(chooseTime.night.time)
                : null,
            },
            {
              onBeginning: () => {
                dispatch(GlobalAction.setShowLoading({isLoading: true}));
              },
              onSuccess: (response: any) => {
                switch (response.errorCode) {
                  case code.success:
                    updateAccessTokenToHeader({
                      accessToken: response.data.access_token,
                    });
                    props.navigation.navigate('BottomTab', {
                      screen: 'Home',
                    });
                    break;
                  case code.unauthorize:
                    props.navigation.navigate(ScreenMap.SignIn, {});
                    break;
                }
              },
              onFailure: (error: any) => {
                console.warn('error', error);
              },
              onFinish: () => {
                dispatch(GlobalAction.setShowLoading({isLoading: false}));
              },
            },
          ),
        );
      }
    },
    [dispatch, props.navigation],
  );

  const renderTimePicker = (
    time: {start: string; end: string},
    time_type: number,
    isStart: boolean,
  ) => {
    const timeStartArr = time.start.split(':');
    const timeEndArr = time.end.split(':');
    return (
      <TouchableOpacity
        onPress={() => {
          setShowDatePicker(true),
            setChooseStart(isStart),
            setChooseTimeType(time_type),
            setTimeEdit(time);
        }}>
        <Text style={styles.textSetTime}>
          {isStart
            ? moment(
                new Date().setHours(+timeStartArr[0], +timeStartArr[1]),
              ).format('HH:mm')
            : moment(
                new Date().setHours(+timeEndArr[0], +timeEndArr[1]),
              ).format('HH:mm')}
        </Text>
      </TouchableOpacity>
    );
  };

  const renderChooseTimeItem = (
    date_time: {ischeck: boolean; time: {start: string; end: string}},
    title: string,
    time_type: number,
  ) => {
    return (
      <View style={styles.itemChooseTime}>
        <TouchableOpacity onPress={() => handle_check(time_type)}>
          <Image
            source={date_time.ischeck ? media.checked : media.unchecked}
            style={styles.iconCheck}
          />
        </TouchableOpacity>
        <View style={styles.groupOpenTime}>
          <Text style={styles.textTime}>{title}</Text>
          <View style={styles.groupTime}>
            <View style={styles.subGroupTime}>
              <Text style={styles.textSubTime}>{'Từ:'}</Text>
              {renderTimePicker(date_time.time, time_type, true)}
              <Image source={media.clock} style={styles.sizeClock} />
            </View>
            <View style={styles.subGroupTime}>
              <Text style={styles.textSubTime}>{'Đến:'}</Text>
              {renderTimePicker(date_time.time, time_type, false)}
              <Image source={media.clock} style={styles.sizeClock} />
            </View>
          </View>
        </View>
      </View>
    );
  };

  const renderChooseTime = () => {
    return (
      <View style={styles.groupChooseTime}>
        <Text style={styles.labelTime}>
          {'Giờ mở bán'}
          <Text style={styles.required}>{' *'}</Text>
        </Text>
        {renderChooseTimeItem(
          chooseTime.morning,
          'Giờ mở cửa buổi sáng (5:00 - 10:00)',
          time_type.morning,
        )}
        {renderChooseTimeItem(
          chooseTime.noon,
          'Giờ mở cửa buổi trưa (10:01 - 14:00)',
          time_type.noon,
        )}
        {renderChooseTimeItem(
          chooseTime.afternoon,
          'Giờ mở cửa buổi chiều (14:01 - 19:00)',
          time_type.afternoon,
        )}
        {renderChooseTimeItem(
          chooseTime.night,
          'Giờ mở cửa buổi tối (19:01 - 23:00)',
          time_type.night,
        )}
      </View>
    );
  };

  const renderDropdown = (
    label: string,
    data: {label: string; value: number}[],
    placeHolder: string,
    zIndex: number,
  ) => {
    return (
      <>
        <Text
          style={[
            styles.label,
            {flex: 1, marginHorizontal: 20, marginTop: 10},
          ]}>
          {label}
          <Text style={styles.required}>{' *'}</Text>
        </Text>
        <DropDownPicker
          zIndex={zIndex}
          items={data}
          placeholder={placeHolder}
          searchable={true}
          searchablePlaceholder="Tìm kiếm"
          searchablePlaceholderTextColor="gray"
          searchableError={() => <Text>{'Không tìm thấy'}</Text>}
          containerStyle={{height: 40, marginHorizontal: 20, marginTop: 10}}
          style={{
            backgroundColor: Palette.white,
            borderWidth: 0,
            borderBottomWidth: 0.5,
          }}
          selectedLabelStyle={{color: Palette.black}}
          activeLabelStyle={{color: Palette.color_ff4}}
          itemStyle={{
            justifyContent: 'flex-start',
            borderBottomWidth: 0.5,
            borderBottomColor: Palette.color_ccc,
          }}
          placeholderStyle={{
            color: Palette.color_ccc,
            fontSize: RFValue(10, 580),
          }}
          dropDownStyle={{backgroundColor: Palette.white}}
          onChangeItem={item => setCategories(item.value)}
        />
      </>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <KeyboardAwareScrollView>
        <View style={styles.formHolder}>
          <View style={styles.form}>
            <View style={styles.groupInput}>
              <Input
                label="Tên cửa hàng"
                image={media.store}
                placeHolder="Nhập tên cửa hàng"
                placeHolderColor={Palette.color_ccc}
                isRequired={true}
                maxLength={40}
                value={shopName}
                setValue={setShopName}
              />
              {validateShopName.length > 0 ? (
                <Text style={styles.validate}>{validateShopName}</Text>
              ) : null}
              <Input
                label="Số điện thoại 1"
                image={media.phone}
                keyboardNumber={true}
                placeHolder="Nhập số điện thoại"
                placeHolderColor={Palette.color_ccc}
                editable={false}
                value={`${dataUser.userName} (số đã đăng ký OTP)`}
              />
              <Input
                label="Số điện thoại 2"
                image={media.phone}
                keyboardNumber={true}
                placeHolder="Nhập số điện thoại"
                placeHolderColor={Palette.color_ccc}
                isRequired={true}
                maxLength={11}
                value={phone}
                setValue={setPhone}
              />
              {validatePhone.length > 0 ? (
                <Text style={styles.validate}>{validatePhone}</Text>
              ) : null}
              {renderDropdown(
                'Cung cấp loại mặt hàng',
                listCategories,
                'Vui lòng chọn ngành hàng bạn đang kinh doanh',
                5000,
              )}
              {validateCategories.length > 0 ? (
                <Text style={styles.validate}>{validateCategories}</Text>
              ) : null}
              <View style={styles.groupImage}>
                <Text style={styles.titleImage}>
                  <Text style={styles.required}>{'* '}</Text>
                  {
                    'Vui lòng cung cấp chứng chỉ, giấy phép, giấy chứng nhận ATTP'
                  }
                </Text>
                <Text style={styles.descriptionImage}>
                  {'(Chỉ chấp nhận ảnh chụp rõ nét, chụp cả 2 mặt của giấy tờ)'}
                </Text>
                <View style={styles.groupImagePicker}>
                  <ImageHolder
                    listImage={listImage}
                    setListValue={setListImage}
                    index={0}
                  />
                  <ImageHolder
                    listImage={listImage}
                    setListValue={setListImage}
                    index={1}
                  />
                  <ImageHolder
                    listImage={listImage}
                    setListValue={setListImage}
                    index={2}
                  />
                  <ImageHolder
                    listImage={listImage}
                    setListValue={setListImage}
                    index={3}
                  />
                </View>
              </View>
              {validateListImage.length > 0 ? (
                <Text style={styles.validate}>{validateListImage}</Text>
              ) : null}
              {renderChooseTime()}
              {validateChooseTime.length > 0 ? (
                <Text style={styles.validate}>{validateChooseTime}</Text>
              ) : null}
            </View>
            <ButtonSubmit
              title="Cập nhật"
              color={[Palette.color_ff0, Palette.color_ff4]}
              action={() => {
                updateShopInfo(
                  shopName,
                  phone,
                  categories,
                  listImage,
                  chooseTime,
                );
              }}
            />
            <DateTimePickerModal
              isVisible={showDatePicker}
              mode="time"
              is24Hour={true}
              display={'spinner'}
              date={
                chooseStart
                  ? new Date(
                      new Date().setHours(
                        +timeEdit.start.split(':')[0],
                        +timeEdit.start.split(':')[1],
                      ),
                    )
                  : new Date(
                      new Date().setHours(
                        +timeEdit.end.split(':')[0],
                        +timeEdit.end.split(':')[1],
                      ),
                    )
              }
              onConfirm={date => {
                setShowDatePicker(false);
                handle_choose_time(
                  moment(date).format('HH:mm'),
                  chooseTimeType,
                  chooseStart,
                );
              }}
              onCancel={() => {
                setShowDatePicker(false);
              }}
            />
          </View>
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};
export default UpdateShopInfoScreen;
