import {Palette} from 'App/Theme/Palette';
import {Platform, StyleSheet} from 'react-native';
import {getBottomSpace} from 'react-native-iphone-x-helper';
import {RFValue} from 'react-native-responsive-fontsize';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Palette.white,
    marginBottom: getBottomSpace(),
  },
  formHolder: {
    flex: 1,
    marginBottom: 20,
  },
  form: {
    flex: 1,
  },
  groupInput: {
    marginTop: 15,
    marginBottom: 30,
    zIndex: Platform.OS !== 'android' ? 10 : 0,
  },
  label: {
    color: Palette.color_545,
    fontSize: RFValue(10, 580),
  },
  containerDate: {
    height: 60,
    marginHorizontal: 30,
    marginTop: 20,
  },
  groupIconDate: {
    flex: 1,
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: Palette.color_ccc,
  },
  icon: {
    width: 13,
    height: 13,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginRight: 13,
  },
  groupRadio: {
    flex: 1,
    maxWidth: 200,
    flexDirection: 'row',
    marginTop: 15,
  },
  groupIconRadio: {
    flex: 1,
    flexDirection: 'row',
  },
  iconRadio: {
    width: 15,
    height: 15,
    marginRight: 10,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  groupChooseTime: {
    flex: 1,
    marginHorizontal: 20,
    justifyContent: 'space-around',
  },
  itemChooseTime: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 25,
  },
  iconCheck: {
    width: 18,
    height: 18,
  },
  textTime: {
    marginLeft: 10,
    color: Palette.color_545,
    fontSize: RFValue(10, 580),
  },
  groupOpenTime: {
    flex: 1,
    flexDirection: 'column',
  },
  groupTime: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  subGroupTime: {
    flex: 1,
    flexDirection: 'row',
    flexGrow: 1,
    maxWidth: 120,
    marginHorizontal: 20,
    borderBottomWidth: 0.5,
    borderBottomColor: Palette.color_ccc,
  },
  dateSize: {
    height: 30,
  },
  textSubTime: {
    alignSelf: 'center',
    marginBottom: 2,
  },
  sizeClock: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    alignSelf: 'center',
    position: 'absolute',
    right: 0,
    bottom: 1,
  },
  groupImage: {
    flex: 1,
    marginTop: 20,
    marginHorizontal: 20,
  },
  titleImage: {
    fontSize: RFValue(9, 580),
    color: Palette.color_1f1,
    lineHeight: 16,
  },
  descriptionImage: {
    fontSize: RFValue(9, 580),
    color: Palette.color_1f1,
    lineHeight: 16,
    fontStyle: 'italic',
    fontWeight: '300',
  },
  groupImagePicker: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 20,
    justifyContent: 'space-around',
  },
  labelInput: {
    fontSize: RFValue(9, 580),
    color: Palette.color_1f1,
    lineHeight: 16,
  },
  input: {
    flex: 1,
    fontSize: RFValue(9, 580),
    color: Palette.color_1f1,
  },
  groupInputType: {
    flex: 1,
    marginVertical: 15,
    marginHorizontal: 20,
    borderBottomWidth: 0.5,
    borderBottomColor: Palette.color_ccc,
  },
  required: {
    fontWeight: '600',
    fontSize: RFValue(12, 580),
    color: Palette.color_ff3,
  },
  labelTime: {
    fontSize: RFValue(10, 580),
    fontWeight: 'bold',
    marginTop: 20,
  },
  validate: {
    color: 'red',
    marginTop: 9,
    marginHorizontal: 20,
  },
  textSetTime: {
    marginLeft: 20,
    marginBottom: 2,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    width: '100%',
    margin: 20,
    paddingTop: 10,
    backgroundColor: 'white',
    alignItems: 'flex-end',
    shadowColor: Palette.black,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.25,
    elevation: 3,
  },
  textDoneChooseTime: {
    fontSize: RFValue(14, 580),
    color: Palette.color_055,
    marginRight: 20,
  },
});
export default styles;
