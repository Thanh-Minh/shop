import {BaseScreenProps} from 'App/@types/screen-type';
import media from 'App/assets/media';
import ButtonIcon from 'App/Components/Form/ButtonIcon';
import ButtonSubmit from 'App/Components/Form/ButtonSubmit';
import Input from 'App/Components/Form/Input';
import PopUpImage from 'App/Components/PopUpImage';
import env from 'App/Config/Enviroment/env';
import {ScreenMap} from 'App/Config/NavigationConfig';
import {code} from 'App/Lib/codeHelpers';
import {convert_dropdown_data} from 'App/Lib/convertDataHelper';
import {createFormData} from 'App/Lib/dataHelper';
import {Actions as DataAction} from 'App/Redux/DataRedux';
import {Actions as GlobalAction} from 'App/Redux/GlobalRedux';
import {Actions as InfoAction} from 'App/Redux/InfoRedux';
import {Palette} from 'App/Theme/Palette';
import moment from 'moment';
import React, {ReactElement, useCallback, useEffect, useState} from 'react';
import {
  ActivityIndicator,
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import FastImage from 'react-native-fast-image';
import ImagePicker from 'react-native-image-crop-picker';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import NumericInput from 'react-native-numeric-input';
import {RFValue} from 'react-native-responsive-fontsize';
import {useDispatch} from 'react-redux';
import styles from './styles';

const ProductPrice = ({
  item,
  discount,
  index,
  deleteProductPrice,
  changeName,
  changeQuantity,
  changePrice,
}: {
  item: {name: string; quantity: number; price: number; type: number};
  discount: number;
  index: number;
  deleteProductPrice: () => void;
  changeName: (index: number, name: string) => void;
  changeQuantity: (index: number, quantity: number) => void;
  changePrice: (index: number, price: string) => void;
}) => (
  <View style={styles.productPriceContainer}>
    <TouchableOpacity onPress={deleteProductPrice}>
      <Image source={media.delete} style={styles.deleteIcon} />
    </TouchableOpacity>
    <View style={styles.groupInput}>
      <View style={styles.itemInput}>
        <Text>{'Đơn vị'}</Text>
        <TextInput
          style={styles.input}
          value={`${item.name}`}
          keyboardType="numeric"
          onChangeText={text => changeName(index, text)}
        />
      </View>
      <View style={styles.itemInputNumeric}>
        <Text>{'Số lượng'}</Text>
        <NumericInput
          initValue={item.quantity}
          totalWidth={80}
          totalHeight={30}
          rounded
          borderColor={Palette.color_e8e}
          rightButtonBackgroundColor={Palette.color_e8e}
          leftButtonBackgroundColor={Palette.color_e8e}
          containerStyle={styles.numberInput}
          onChange={value => changeQuantity(index, value)}
        />
      </View>
      <View style={styles.itemInput}>
        <Text>{'Giá bán'}</Text>
        <TextInput
          style={styles.input}
          keyboardType="numeric"
          value={item.price + ''}
          onChangeText={text => changePrice(index, text)}
        />
      </View>
    </View>
    <View style={styles.groupInput}>
      <Text>{'Giá bán cho người dùng'}</Text>
      <Text style={styles.price}>{`${item.price -
        (item.price * discount) / 100}`}</Text>
    </View>
  </View>
);

const CreateProductScreen = (
  props: BaseScreenProps<ScreenMap.CreateProduct>,
): ReactElement => {
  const dispatch = useDispatch();
  const [image, setImage] = useState('');
  const [name, setName] = useState('');
  const [note, setNote] = useState('');
  const [groupId, setGroupId] = useState(-1);
  const [listGroups, setListGroups] = useState([
    {
      label: 'Dữ liệu hiện tại chưa có',
      value: -1,
    },
  ]);
  const [classId, setClassId] = useState(-1);
  const [listClass, setListClass] = useState([
    {
      label: 'Dữ liệu hiện tại chưa có',
      value: -1,
    },
  ]);
  const [categoryId, setCategoryId] = useState(-1);
  const [listCategories, setListCategories] = useState([
    {
      label: 'Dữ liệu hiện tại chưa có',
      value: -1,
    },
  ]);
  const [unitId, setUnitId] = useState(-1);
  const [listUnits, setListUnits] = useState([
    {
      label: 'Dữ liệu hiện tại chưa có',
      value: -1,
    },
  ]);
  const [basePrice, setBasePrice] = useState('');
  const [discount, setDiscount] = useState('');
  const [companyDiscount, setCompanyDiscount] = useState('');
  const [expiredDate, setExpiredDate] = useState('Vui lòng chọn ngày hết hạn');
  const [description, setDescription] = useState('');
  const [loading, setLoading] = useState(false);
  const [showDatePicker, setShowDatePicker] = useState(false);

  const [chooseVisible, setChooseVisible] = useState(false);
  const [dataProductPrice, setDataProductPrice] = useState([
    {name: '', quantity: 0, price: 0, type: 0},
  ]);

  useEffect(() => {
    const unsubcribe = props.navigation.addListener('focus', () => {
      dispatch(
        DataAction.getCategories(
          {},
          {
            onBeginning: () => {},
            onSuccess: (response: any) => {
              setListGroups(convert_dropdown_data(response.data));
            },
            onFailure: () => {},
            onFinish: () => {},
          },
        ),
      );
    });
    return unsubcribe;
  }, [dispatch, props.navigation]);

  const getListSubCategories = useCallback(
    (zIndex: number, categoryId: number) => {
      dispatch(
        DataAction.getSubCategories(
          {categoryId: categoryId},
          {
            onBeginning: () => {},
            onSuccess: (response: any) => {
              switch (zIndex) {
                case 5000:
                  setGroupId(categoryId);
                  setClassId(-1);
                  setCategoryId(-1);
                  setUnitId(-1);
                  setListClass(convert_dropdown_data(response.data));
                  break;
                case 4000:
                  setClassId(categoryId);
                  setCategoryId(-1);
                  setUnitId(-1);
                  setListCategories(convert_dropdown_data(response.data));
                  break;
              }
            },
            onFailure: (error: any) => {
              console.warn(error);
            },
            onFinish: () => {},
          },
        ),
      );
    },
    [dispatch],
  );

  const getListUnits = useCallback(
    (categoryId: number) => {
      dispatch(
        DataAction.getListUnits(
          {categoryId: categoryId},
          {
            onBeginning: () => {},
            onSuccess: (response: any) => {
              setCategoryId(categoryId);
              setUnitId(-1);
              setListUnits(convert_dropdown_data(response.data));
            },
            onFailure: (error: any) => {
              console.warn(error);
            },
            onFinish: () => {},
          },
        ),
      );
    },
    [dispatch],
  );

  const deleteProductPrice = (index: number) => {
    dataProductPrice.splice(index, 1);
    setDataProductPrice([...dataProductPrice]);
  };

  const changeName = (index: number, name: string) => {
    dataProductPrice[index].name = name;
    setDataProductPrice([...dataProductPrice]);
  };

  const changeQuantity = (index: number, quantity: number) => {
    dataProductPrice[index].quantity = quantity;
    setDataProductPrice([...dataProductPrice]);
  };

  const changePrice = (index: number, price: string) => {
    dataProductPrice[index].price = +price;
    setDataProductPrice([...dataProductPrice]);
  };

  const renderProductPrice = ({
    item,
    index,
  }: {
    item: {name: string; quantity: number; price: number; type: number};
    index: number;
  }) => (
    <ProductPrice
      item={item}
      discount={+discount}
      index={index}
      deleteProductPrice={() => deleteProductPrice(index)}
      changeName={changeName}
      changeQuantity={changeQuantity}
      changePrice={changePrice}
    />
  );

  const setDefaulValueDrop = (zIndex: number) => {
    let data = null;
    switch (zIndex) {
      case 4000:
        data = classId == -1 ? null : classId;
        break;
      case 3000:
        data = categoryId == -1 ? null : categoryId;
        break;
      case 2000:
        data = unitId == -1 ? null : unitId;
        break;
    }
    return data;
  };

  const renderDropdown = (
    label: string,
    data: {label: string; value: number}[],
    placeHolder: string,
    zIndex: number,
  ) => {
    return (
      <>
        <Text
          style={[
            styles.label,
            {flex: 1, marginHorizontal: 20, marginTop: 10},
          ]}>
          {label}
          <Text style={styles.required}>{' *'}</Text>
        </Text>
        <DropDownPicker
          zIndex={zIndex}
          items={data}
          placeholder={placeHolder}
          defaultValue={setDefaulValueDrop(zIndex)}
          searchable={true}
          searchablePlaceholder="Tìm kiếm"
          searchablePlaceholderTextColor="gray"
          searchableError={() => <Text>{'Không tìm thấy'}</Text>}
          containerStyle={{height: 40, marginHorizontal: 20, marginTop: 10}}
          style={{
            backgroundColor: Palette.white,
            borderWidth: 0,
            borderBottomWidth: 0.5,
          }}
          selectedLabelStyle={{color: Palette.black}}
          activeLabelStyle={{color: Palette.color_ff4}}
          itemStyle={{
            justifyContent: 'flex-start',
            borderBottomWidth: 0.5,
            borderBottomColor: Palette.color_ccc,
          }}
          placeholderStyle={{
            color: Palette.color_ccc,
            fontSize: RFValue(10, 580),
          }}
          dropDownStyle={{backgroundColor: Palette.white}}
          onChangeItem={item => {
            switch (zIndex) {
              case 5000:
                getListSubCategories(zIndex, item.value);
                break;
              case 4000:
                getListSubCategories(zIndex, item.value);
                break;
              case 3000:
                getListUnits(item.value);
                break;
              case 2000:
                setUnitId(item.value);
                break;
            }
          }}
        />
      </>
    );
  };

  const renderDatePicker = () => {
    return (
      <View style={styles.containerDate}>
        <Text style={styles.label}>{'Hạn dùng sản phẩm'}</Text>
        <View style={styles.groupIconDate}>
          <TouchableOpacity
            style={{flex: 1, alignSelf: 'center'}}
            onPress={() => {
              setShowDatePicker(true);
            }}>
            <Text
              style={{
                fontSize: RFValue(10, 580),
                color: expiredDate.includes('-')
                  ? Palette.black
                  : Palette.color_ccc,
              }}>
              {expiredDate}
            </Text>
          </TouchableOpacity>
          <DateTimePickerModal
            isVisible={showDatePicker}
            mode="date"
            is24Hour={true}
            display={'spinner'}
            date={new Date()}
            onConfirm={(date: any) => {
              setShowDatePicker(false);
              setExpiredDate(moment(date).format('DD-MM-YYYY'));
            }}
            onCancel={() => {
              setShowDatePicker(false);
            }}
          />
        </View>
      </View>
    );
  };

  const addProductPrice = () => {
    setDataProductPrice([
      ...dataProductPrice,
      {
        name: '',
        quantity: 0,
        price: 0,
        type: 0,
      },
    ]);
  };

  const uploadImage = (img: any) => {
    dispatch(
      DataAction.uploadImage(
        {formData: createFormData(img)},
        {
          onBeginning: () => {
            setLoading(true);
          },
          onSuccess: (response: any) => {
            setImage(response.data[0]);
          },
          onFailure: (error: any) => {
            console.warn(error);
          },
          onFinish: () => {
            setLoading(false);
          },
        },
      ),
    );
  };

  const open_gallery = () => {
    setChooseVisible(false);
    setTimeout(() => {
      ImagePicker.openPicker({
        width: 300,
        height: 400,
        cropping: true,
      })
        .then(img => {
          uploadImage(img);
        })
        .catch(err => {
          console.warn(err);
        });
    }, 200);
  };

  const open_camera = () => {
    setChooseVisible(false);
    setTimeout(() => {
      ImagePicker.openCamera({
        width: 300,
        height: 400,
        cropping: true,
      })
        .then(img => {
          uploadImage(img);
        })
        .catch(err => {
          console.warn(err);
        });
    }, 200);
  };

  const validate_data = (
    image: string,
    name: string,
    note: string,
    groupId: number,
    classId: number,
    categoryId: number,
    unitId: number,
    basePrice: number,
    discount: number,
    companyDiscount: number,
    expiredDate: number,
    description: string,
    dataProductPrice: {
      name: string;
      quantity: number;
      price: number;
      type: number;
    }[],
  ) => {
    let validate_fail = 0;

    return validate_fail == 0;
  };

  const createProduct = useCallback(
    (
      image,
      name,
      note,
      groupId,
      classId,
      categoryId,
      unitId,
      basePrice,
      discount,
      companyDiscount,
      expiredDate,
      description,
      dataProductPrice,
    ) => {
      if (
        validate_data(
          image,
          name,
          note,
          groupId,
          classId,
          categoryId,
          unitId,
          basePrice,
          discount,
          companyDiscount,
          expiredDate,
          description,
          dataProductPrice,
        )
      ) {
        dispatch(
          InfoAction.createProduct(
            {
              image: image,
              name: name,
              note: note,
              groupId: groupId,
              classId: classId,
              categoryId: categoryId,
              unitId: unitId,
              basePrice: +basePrice,
              discount: +discount,
              companyDiscount: +companyDiscount,
              expiredDate: new Date(
                moment(expiredDate, 'DD-MM-YYYY').format('YYYY-MM-DD'),
              ).getTime(),
              description: description,
              dataProductPrice: dataProductPrice,
            },
            {
              onBeginning: () => {
                dispatch(GlobalAction.setShowLoading({isLoading: true}));
              },
              onSuccess: (response: any) => {
                switch (response.errorCode) {
                  case code.success:
                    props.navigation.navigate(ScreenMap.Home, {});
                    break;
                  case code.unauthorize:
                    props.navigation.navigate(ScreenMap.SignIn, {});
                    break;
                }
              },
              onFailure: (error: any) => {
                console.warn('error', error);
              },
              onFinish: () => {
                dispatch(GlobalAction.setShowLoading({isLoading: false}));
              },
            },
          ),
        );
      }
    },
    [dispatch, props.navigation],
  );

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.container}>
        <View style={styles.groupImage}>
          <TouchableOpacity onPress={() => setChooseVisible(true)}>
            <View style={styles.containerImage}>
              {loading ? (
                <ActivityIndicator style={styles.loading} animating={true} />
              ) : null}
              <FastImage
                source={
                  image
                    ? {uri: env.API_ENDPOINT + image}
                    : media.add_image_product
                }
                style={image ? styles.image : styles.iconAdd}
                onLoadStart={() => (image ? setLoading(true) : null)}
                onLoadEnd={() => (image ? setLoading(false) : null)}
              />
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.infoHolder}>
          <Input
            label="Tên sản phẩm"
            placeHolder="Nhập tên sản phẩm"
            placeHolderColor={Palette.color_ccc}
            isRequired={true}
            maxLength={40}
            value={name}
            setValue={setName}
          />
          <Input
            label="Chú thích"
            placeHolder="Nhập chú thích"
            placeHolderColor={Palette.color_ccc}
            maxLength={40}
            value={note}
            setValue={setNote}
          />
          {renderDropdown(
            'Danh mục',
            listGroups,
            'Vui lòng chọn loại sản phẩm',
            5000,
          )}
          {renderDropdown(
            'Phân loại',
            listClass,
            'Vui lòng chọn loại sản phẩm',
            4000,
          )}
          {renderDropdown(
            'Sản phẩm',
            listCategories,
            'Vui lòng chọn loại sản phẩm',
            3000,
          )}
          {renderDropdown(
            'Đơn vị tính',
            listUnits,
            'Vui lòng chọn đơn vị tính',
            2000,
          )}
          <Input
            label="Giá chuẩn"
            placeHolder="Vui lòng nhập giá chuẩn"
            placeHolderColor={Palette.color_ccc}
            keyboardNumber={true}
            isRequired={true}
            value={basePrice}
            setValue={setBasePrice}
          />
          <Input
            label="Giảm giá cho người dùng(%)"
            placeHolder="Vui lòng nhập giảm giá"
            placeHolderColor={Palette.color_ccc}
            keyboardNumber={true}
            value={discount}
            setValue={setDiscount}
          />
          <Input
            label="Chiết khấu cho công ty(%)"
            placeHolder="Vui lòng nhập chiết khấu"
            placeHolderColor={Palette.color_ccc}
            keyboardNumber={true}
            value={companyDiscount}
            setValue={setCompanyDiscount}
          />
          {renderDatePicker()}
          <View style={styles.descripHolder}>
            <Text style={styles.titleDes}>{'Mô tả sản phẩm'}</Text>
            <TextInput
              multiline={true}
              value={description}
              maxLength={200}
              onChangeText={text => setDescription(text)}
            />
          </View>
        </View>
        <FlatList data={dataProductPrice} renderItem={renderProductPrice} />
        {dataProductPrice.length < 5 ? (
          <View style={styles.btnAddProductPrice}>
            <ButtonIcon
              title="Thêm"
              image={media.add_product}
              color={[Palette.color_ff0, Palette.color_ff4]}
              action={() => addProductPrice()}
            />
          </View>
        ) : null}
        <View style={styles.desHolder}>
          <ButtonSubmit
            title="Cập nhật"
            color={[Palette.color_ff0, Palette.color_ff4]}
            action={() =>
              createProduct(
                image,
                name,
                note,
                groupId,
                classId,
                categoryId,
                unitId,
                basePrice,
                discount,
                companyDiscount,
                expiredDate,
                description,
                dataProductPrice,
              )
            }
          />
        </View>
        <PopUpImage
          visible={chooseVisible}
          setVisible={setChooseVisible}
          openCamera={() => open_camera()}
          openGallery={() => open_gallery()}
        />
      </ScrollView>
    </SafeAreaView>
  );
};
export default CreateProductScreen;
