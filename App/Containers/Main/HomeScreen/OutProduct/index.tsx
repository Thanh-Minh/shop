import {useNavigation} from '@react-navigation/native';
import ButtonSubmit from 'App/Components/Form/ButtonSubmit';
import env from 'App/Config/Enviroment/env';
import {ScreenMap} from 'App/Config/NavigationConfig';
import {numberWithDots} from 'App/Lib/convertDataHelper';
import {Palette} from 'App/Theme/Palette';
import moment from 'moment';
import React, {ReactElement} from 'react';
import {
  FlatList,
  SafeAreaView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import styles from './styles';

const Item = ({item}: {item: ProductChildItem}) => {
  const navigation = useNavigation();

  return (
    <View style={styles.itemHolder}>
      <TouchableOpacity
        style={styles.contentHolder}
        onPress={() => {
          navigation.navigate('BottomTab', {
            screen: 'Home',
            params: {
              screen:
                item.type == 1
                  ? ScreenMap.UpdateCombo
                  : ScreenMap.UpdateProduct,
              params: {productId: item.productId},
            },
          });
        }}>
        <FastImage
          source={{uri: env.API_ENDPOINT + item.images[0]}}
          style={styles.productImage}
        />
        <Text style={styles.textContent}>{item.productName}</Text>
        <Text style={styles.textContent}>
          {'Hạn dùng ' + moment(item.expiredTime, 'x').format('DD/MM/YYYY')}
        </Text>
        <Text style={[styles.textContent, styles.price]}>
          {item.type == 1
            ? numberWithDots(item.price - (item.price * item.discount) / 100) +
              '/ combo'
            : numberWithDots(item.price - (item.price * item.discount) / 100) +
              '/' +
              item.unitName}
        </Text>
      </TouchableOpacity>
      <ButtonSubmit
        title="Cập nhật"
        color={[Palette.color_ff0, Palette.color_ff4]}
        isProductItem={true}
        action={() => {
          navigation.navigate('BottomTab', {
            screen: 'Home',
            params: {
              screen:
                item.type == 1
                  ? ScreenMap.UpdateCombo
                  : ScreenMap.UpdateProduct,
              params: {productId: item.productId},
            },
          });
        }}
      />
    </View>
  );
};

const OutProduct = ({data, jumpTo}: {data: any; jumpTo: any}): ReactElement => {
  const renderItem = ({item}: {item: ProductChildItem}) => {
    return <Item item={item} />;
  };

  return (
    <SafeAreaView style={styles.container}>
      <FlatList data={data} numColumns={2} renderItem={renderItem} />
    </SafeAreaView>
  );
};
export default OutProduct;
