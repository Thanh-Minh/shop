import {Palette} from 'App/Theme/Palette';
import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Palette.color_fbf,
  },
  contentHolder: {
    flex: 0.95,
  },
  itemHolder: {
    flex: 0.5,
    margin: 10,
    backgroundColor: Palette.white,
    paddingBottom: 15,
    borderRadius: 10,
    shadowColor: Palette.black,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  textContent: {
    fontSize: RFValue(10, 580),
    marginHorizontal: 8,
    marginTop: 5,
  },
  productImage: {
    marginTop: 10,
    width: '90%',
    height: 130,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginBottom: 10,
  },
  price: {
    color: Palette.color_ff1,
    fontWeight: '600',
  },
});
export default styles;
