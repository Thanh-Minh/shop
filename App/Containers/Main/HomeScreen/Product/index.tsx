import {useNavigation} from '@react-navigation/native';
import ButtonSubmit from 'App/Components/Form/ButtonSubmit';
import env from 'App/Config/Enviroment/env';
import {ScreenMap} from 'App/Config/NavigationConfig';
import {code} from 'App/Lib/codeHelpers';
import {numberWithDots} from 'App/Lib/convertDataHelper';
import {Actions as GlobalAction} from 'App/Redux/GlobalRedux';
import {Actions as InfoAction} from 'App/Redux/InfoRedux';
import {Palette} from 'App/Theme/Palette';
import moment from 'moment';
import React, {ReactElement, useCallback, useState} from 'react';
import {
  FlatList,
  SafeAreaView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import AwesomeAlert from 'react-native-awesome-alerts';
import FastImage from 'react-native-fast-image';
import NumericInput from 'react-native-numeric-input';
import {useDispatch} from 'react-redux';
import styles from './styles';

const Item = ({
  item,
  outStock,
}: {
  item: ProductChildItem;
  outStock: () => void;
}) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [quantity, setQuantity] = useState(item.quantity);

  const updateQuantity = useCallback(() => {
    dispatch(
      InfoAction.setProductQuantity(
        {productId: item.id, quantity: quantity},
        {
          onBeginning: () => {
            dispatch(GlobalAction.setShowLoading({isLoading: true}));
          },
          onSuccess: (response: any) => {
            switch (response.errorCode) {
              case code.success:
                navigation.navigate(ScreenMap.Home, {});
                break;
              case code.unauthorize:
                navigation.navigate(ScreenMap.SignIn, {});
                break;
            }
          },
          onFailure: (error: any) => {
            console.warn(error);
          },
          onFinish: () => {
            dispatch(GlobalAction.setShowLoading({isLoading: false}));
          },
        },
      ),
    );
  }, [dispatch, quantity]);

  return (
    <View style={styles.itemHolder}>
      <TouchableOpacity
        style={styles.contentHolder}
        onPress={() => {
          navigation.navigate('BottomTab', {
            screen: 'Home',
            params: {
              screen:
                item.type == 1
                  ? ScreenMap.UpdateCombo
                  : ScreenMap.UpdateProduct,
              params: {productId: item.productId},
            },
          });
        }}>
        <FastImage
          source={{uri: env.API_ENDPOINT + item.images[0]}}
          style={styles.productImage}
        />
        <Text style={styles.textContent}>{item.productName}</Text>
        <Text style={styles.textContent}>
          {'Hạn dùng ' + moment(item.expiredTime, 'x').format('DD/MM/YYYY')}
        </Text>
        <Text style={[styles.textContent, styles.price]}>
          {item.type == 1
            ? numberWithDots(item.price - (item.price * item.discount) / 100) +
              '/ combo'
            : numberWithDots(item.price - (item.price * item.discount) / 100) +
              '/' +
              item.unitName}
        </Text>
      </TouchableOpacity>
      <View style={styles.amountHolder}>
        <Text style={styles.textAmount}>{'SL còn'}</Text>
        <NumericInput
          initValue={quantity}
          totalWidth={80}
          totalHeight={30}
          rounded
          borderColor={Palette.color_e8e}
          rightButtonBackgroundColor={Palette.color_e8e}
          leftButtonBackgroundColor={Palette.color_e8e}
          containerStyle={styles.numberInput}
          onChange={value => setQuantity(value)}
        />
      </View>
      <ButtonSubmit
        title={quantity != item.quantity ? 'Cập nhật' : 'Báo hết hàng'}
        color={[Palette.color_ff0, Palette.color_ff4]}
        isProductItem={true}
        action={quantity != item.quantity ? updateQuantity : outStock}
      />
    </View>
  );
};

const Product = ({data, jumpTo}: {data: any; jumpTo: any}): ReactElement => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [showAlert, setShowAlert] = useState(false);
  const [productId, setProductId] = useState(0);

  const setProductOutStock = useCallback(
    (productId: number) => {
      dispatch(
        InfoAction.setProductOutStock(
          {productId: productId},
          {
            onBeginning: () => {
              dispatch(GlobalAction.setShowLoading({isLoading: true}));
            },
            onSuccess: (response: any) => {
              switch (response.errorCode) {
                case code.success:
                  jumpTo('outproduct');
                  break;
                case code.unauthorize:
                  navigation.navigate(ScreenMap.SignIn, {});
                  break;
              }
              setShowAlert(false);
            },
            onFailure: (error: any) => {
              console.warn(error);
            },
            onFinish: () => {
              dispatch(GlobalAction.setShowLoading({isLoading: false}));
            },
          },
        ),
      );
    },
    [dispatch],
  );

  const renderItem = ({item}: {item: ProductChildItem}) => {
    return (
      <Item
        item={item}
        outStock={() => {
          setProductId(item.id), setShowAlert(true);
        }}
      />
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <FlatList data={data} numColumns={2} renderItem={renderItem} />
      <AwesomeAlert
        show={showAlert}
        showProgress={false}
        title="Xác Nhận"
        message="Bạn có chắc là sản phẩm hết hàng?"
        closeOnTouchOutside={true}
        closeOnHardwareBackPress={false}
        showCancelButton={true}
        showConfirmButton={true}
        cancelText="Hủy"
        confirmText="Xác Nhận"
        confirmButtonColor="#DD6B55"
        onCancelPressed={() => {
          setShowAlert(false);
        }}
        onConfirmPressed={() => {
          setProductOutStock(productId);
        }}
      />
    </SafeAreaView>
  );
};
export default Product;
