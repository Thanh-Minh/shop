import {BaseScreenProps} from 'App/@types/screen-type';
import media from 'App/assets/media';
import {ScreenMap} from 'App/Config/NavigationConfig';
import {screenWidth} from 'App/Config/ScreenConfig';
import {code} from 'App/Lib/codeHelpers';
import {Actions as DataAction} from 'App/Redux/DataRedux';
import {Palette} from 'App/Theme/Palette';
import React, {ReactElement, useEffect, useState} from 'react';
import {Image, View} from 'react-native';
import ActionButton from 'react-native-action-button';
import {TabBar, TabView} from 'react-native-tab-view';
import {useDispatch} from 'react-redux';
import OutProduct from './OutProduct';
import Product from './Product';
import styles from './styles';

const initialLayout = {width: screenWidth};

const HomeScreen = (props: BaseScreenProps<ScreenMap.Home>): ReactElement => {
  const dispatch = useDispatch();
  const [index, setIndex] = useState(0);
  const [routes] = useState([
    {key: 'product', title: 'Đang bán'},
    {key: 'outproduct', title: 'Hết hàng'},
  ]);

  const [data, setData] = useState([]);

  useEffect(() => {
    const unsubcribe = props.navigation.addListener('focus', () => {
      setIndex(0);
      getList(true);
    });
    return unsubcribe;
  }, [dispatch, props.navigation]);

  const getList = (status: boolean) => {
    dispatch(
      DataAction.getListProduct(
        {isOnSale: status},
        {
          onBeginning: () => {},
          onSuccess: (response: any) => {
            switch (response.errorCode) {
              case code.success:
                setData(response.data.data);
                break;
              case code.unauthorize:
                props.navigation.navigate(ScreenMap.SignIn, {});
                break;
            }
          },
          onFailure: (error: any) => {
            console.log(error);
          },
          onFinish: () => {},
        },
      ),
    );
  };

  const onChangetab = (index: number) => {
    setIndex(index);
    setData([]);
    getList(index == 0);
  };

  const renderScene = ({route, jumpTo}: {route: any; jumpTo: any}) => {
    switch (route.key) {
      case 'product':
        return <Product data={data} jumpTo={jumpTo} />;
      case 'outproduct':
        return <OutProduct data={data} jumpTo={jumpTo} />;
      default:
        return null;
    }
  };

  const renderTabBar = (props: any) => (
    <TabBar
      {...props}
      getLabelText={({route}) => route.title}
      inactiveColor={Palette.black}
      activeColor={Palette.color_e91}
      indicatorStyle={{backgroundColor: Palette.white}}
      style={{backgroundColor: Palette.white}}
    />
  );

  return (
    <View style={styles.container}>
      <TabView
        renderTabBar={renderTabBar}
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={index => onChangetab(index)}
        initialLayout={initialLayout}
      />
      {index == 0 ? (
        <ActionButton buttonColor="rgba(231,76,60,1)">
          <ActionButton.Item
            buttonColor="rgba(231,76,60,1)"
            title="Thêm Combo"
            onPress={() => {
              props.navigation.navigate(ScreenMap.CreateCombo, {});
            }}>
            <Image
              source={media.combo}
              style={{
                width: 55,
                height: 55,
                resizeMode: 'cover',
              }}
            />
          </ActionButton.Item>
          <ActionButton.Item
            buttonColor="rgba(231,76,60,1)"
            title="Thêm sản phẩm"
            onPress={() => {
              props.navigation.navigate(ScreenMap.CreateProduct, {});
            }}>
            <Image
              source={media.add_product}
              style={{
                width: 30,
                height: 30,
                resizeMode: 'contain',
              }}
            />
          </ActionButton.Item>
        </ActionButton>
      ) : null}
    </View>
  );
};
export default HomeScreen;
