import {BaseScreenProps} from 'App/@types/screen-type';
import media from 'App/assets/media';
import ButtonIcon from 'App/Components/Form/ButtonIcon';
import ButtonSubmit from 'App/Components/Form/ButtonSubmit';
import Input from 'App/Components/Form/Input';
import PopUpImage from 'App/Components/PopUpImage';
import env from 'App/Config/Enviroment/env';
import {ScreenMap} from 'App/Config/NavigationConfig';
import {code} from 'App/Lib/codeHelpers';
import {createFormData} from 'App/Lib/dataHelper';
import {Actions as DataAction} from 'App/Redux/DataRedux';
import {Actions as GlobalAction} from 'App/Redux/GlobalRedux';
import {Actions as InfoAction} from 'App/Redux/InfoRedux';
import {Palette} from 'App/Theme/Palette';
import moment from 'moment';
import React, {ReactElement, useCallback, useEffect, useState} from 'react';
import {
  ActivityIndicator,
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import ImagePicker from 'react-native-image-crop-picker';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import NumericInput from 'react-native-numeric-input';
import {RFValue} from 'react-native-responsive-fontsize';
import {useDispatch} from 'react-redux';
import styles from './styles';

const ProductPrice = ({
  item,
  index,
  deleteProductPrice,
  changePrice,
  changeComboSize,
  changeQuantity,
}: {
  item: {name: string; comboSize: number; price: number; quantity: number};
  index: number;
  deleteProductPrice: () => void;
  changePrice: (index: number, price: string) => void;
  changeComboSize: (index: number, comboSize: number) => void;
  changeQuantity: (index: number, quantity: number) => void;
}) => (
  <View style={styles.productPriceContainer}>
    <TouchableOpacity onPress={deleteProductPrice}>
      <Image source={media.delete} style={styles.deleteIcon} />
    </TouchableOpacity>
    <View style={styles.groupInput}>
      <View style={styles.itemInput}>
        <Text style={styles.titleInput}>{'Giá combo'}</Text>
        <TextInput
          style={styles.input}
          value={item.price + ''}
          keyboardType="numeric"
          onChangeText={text => changePrice(index, text)}
        />
      </View>
      <View style={styles.itemInputNumeric}>
        <Text style={styles.titleInputNumberic}>{'Sản phẩm'}</Text>
        <NumericInput
          initValue={item.comboSize}
          totalWidth={80}
          totalHeight={30}
          rounded
          borderColor={Palette.color_e8e}
          rightButtonBackgroundColor={Palette.color_e8e}
          leftButtonBackgroundColor={Palette.color_e8e}
          onChange={value => changeComboSize(index, value)}
        />
      </View>
      <View style={styles.itemInputNumeric}>
        <Text style={styles.titleInputNumberic}>{'Số lượng'}</Text>
        <NumericInput
          initValue={item.quantity}
          totalWidth={80}
          totalHeight={30}
          rounded
          borderColor={Palette.color_e8e}
          rightButtonBackgroundColor={Palette.color_e8e}
          leftButtonBackgroundColor={Palette.color_e8e}
          onChange={value => changeQuantity(index, value)}
        />
      </View>
    </View>
  </View>
);

const Product = ({
  item,
  index,
  toggleSelectProduct,
  deleteProduct,
  changeImageProduct,
  changeNameProduct,
  changeNoteProduct,
}: {
  item: {
    image: string;
    name: string;
    note: string;
    isActive: boolean;
  };
  index: number;
  toggleSelectProduct: () => void;
  deleteProduct: () => void;
  changeImageProduct: (index: number, image: string) => void;
  changeNameProduct: (index: number, name: string) => void;
  changeNoteProduct: (index: number, note: string) => void;
}) => {
  const dispatch = useDispatch();
  const [chooseVisible, setChooseVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [image, setImage] = useState('');

  const open_gallery = () => {
    setChooseVisible(false);
    setTimeout(() => {
      ImagePicker.openPicker({
        width: 300,
        height: 400,
        cropping: true,
      })
        .then(img => {
          uploadImage(img);
        })
        .catch(err => {
          console.warn(err);
        });
    }, 200);
  };

  const open_camera = () => {
    setChooseVisible(false);
    setTimeout(() => {
      ImagePicker.openCamera({
        width: 300,
        height: 400,
        cropping: true,
      })
        .then(img => {
          uploadImage(img);
        })
        .catch(err => {
          console.warn(err);
        });
    }, 200);
  };

  const uploadImage = (img: any) => {
    dispatch(
      DataAction.uploadImage(
        {formData: createFormData(img)},
        {
          onBeginning: () => {
            setLoading(true);
          },
          onSuccess: (response: any) => {
            setImage(response.data[0]);
            changeImageProduct(index, response.data[0]);
          },
          onFailure: (error: any) => {
            console.warn(error);
          },
          onFinish: () => {
            setLoading(false);
          },
        },
      ),
    );
  };

  return (
    <View style={{padding: 15}}>
      <TouchableOpacity onPress={deleteProduct}>
        <Image source={media.delete} style={styles.deleteIcon} />
      </TouchableOpacity>
      <View style={styles.productContainer}>
        <TouchableOpacity onPress={() => setChooseVisible(true)}>
          <View style={styles.containerProductImage}>
            {loading ? (
              <ActivityIndicator style={styles.loading} animating={true} />
            ) : null}
            <FastImage
              source={
                item.image.length > 0 && image.length == 0
                  ? {
                      uri: env.API_ENDPOINT + item.image,
                    }
                  : image
                  ? {
                      uri: env.API_ENDPOINT + image,
                    }
                  : media.add
              }
              style={
                item.image.length > 0 && image.length == 0
                  ? styles.imageProduct
                  : image
                  ? styles.imageProduct
                  : styles.iconProductAdd
              }
              onLoadStart={() => setLoading(true)}
              onLoadEnd={() => setLoading(false)}
            />
          </View>
        </TouchableOpacity>
        <View style={styles.groupInfoProduct}>
          <View style={styles.itemInputProduct}>
            <TextInput
              placeholder={'Tên sản phẩm'}
              style={styles.input}
              value={item.name + ''}
              onChangeText={text => changeNameProduct(index, text)}
            />
          </View>
          <View style={styles.itemInputProduct}>
            <TextInput
              placeholder={'Mô tả'}
              style={styles.input}
              value={item.note + ''}
              onChangeText={text => changeNoteProduct(index, text)}
            />
          </View>
        </View>
        <TouchableOpacity
          onPress={toggleSelectProduct}
          style={styles.groupIconCheck}>
          <Image
            source={item.isActive ? media.checked : media.unchecked}
            style={styles.iconCheck}
          />
        </TouchableOpacity>
      </View>
      <PopUpImage
        visible={chooseVisible}
        setVisible={setChooseVisible}
        openCamera={() => open_camera()}
        openGallery={() => open_gallery()}
      />
    </View>
  );
};

const UpdateComboScreen = (
  props: BaseScreenProps<ScreenMap.UpdateCombo>,
): ReactElement => {
  const dispatch = useDispatch();
  const productId = props.route.params.productId;
  const [id, setId] = useState('');
  const [image, setImage] = useState('');
  const [name, setName] = useState('');
  const [note, setNote] = useState('');
  const [discount, setDiscount] = useState('');
  const [companyDiscount, setCompanyDiscount] = useState('');
  const [expiredDate, setExpiredDate] = useState('Vui lòng chọn ngày hết hạn');
  const [description, setDescription] = useState('');
  const [chooseVisible, setChooseVisible] = useState(false);
  const [dataProductPrice, setDataProductPrice] = useState([
    {name: '', comboSize: 0, price: 0, quantity: 0},
  ]);
  const [dataProduct, setDataProduct] = useState([
    {
      image: '',
      name: '',
      note: '',
      isActive: false,
    },
  ]);
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const unsubcribe = props.navigation.addListener('focus', () => {
      dispatch(
        DataAction.getProductById(
          {productId: productId},
          {
            onBeginning: () => {
              setLoading(true);
            },
            onSuccess: (response: any) => {
              const productInfo = response.data;
              setId(productInfo.id);
              setImage(productInfo.images[0]);
              setName(productInfo.name);
              setNote(productInfo.note);
              setDiscount(`${productInfo.discount}`);
              setCompanyDiscount(`${productInfo.companyDiscount}`);
              setExpiredDate(
                moment(productInfo.expiredTime, 'x').format('DD-MM-YYYY'),
              );
              setDescription(productInfo.description);
              setDataProductPrice(productInfo.listDetailProdMongo);
              setDataProduct(productInfo.listComboProduct);
            },
            onFailure: (error: any) => {
              console.warn(error);
            },
            onFinish: () => {
              setLoading(false);
            },
          },
        ),
      );
    });
    return unsubcribe;
  }, [dispatch, props.navigation]);

  const renderProductPrice = ({
    item,
    index,
  }: {
    item: {name: string; comboSize: number; price: number; quantity: number};
    index: number;
  }) => (
    <ProductPrice
      item={item}
      index={index}
      deleteProductPrice={() => deleteProductPrice(index)}
      changePrice={changePrice}
      changeComboSize={changeComboSize}
      changeQuantity={changeQuantity}
    />
  );

  const renderProduct = ({
    item,
    index,
  }: {
    item: {
      image: string;
      name: string;
      note: string;
      isActive: boolean;
    };
    index: number;
  }) => (
    <Product
      item={item}
      index={index}
      toggleSelectProduct={() => toggleSelectProduct(index)}
      deleteProduct={() => deleteProduct(index)}
      changeImageProduct={changeImageProduct}
      changeNameProduct={changeNameProduct}
      changeNoteProduct={changeNoteProduct}
    />
  );

  const renderDatePicker = () => {
    return (
      <View style={styles.containerDate}>
        <Text style={styles.label}>{'Hạn dùng sản phẩm'}</Text>
        <View style={styles.groupIconDate}>
          <TouchableOpacity
            style={{flex: 1, alignSelf: 'center'}}
            onPress={() => {
              setShowDatePicker(true);
            }}>
            <Text
              style={{
                fontSize: RFValue(10, 580),
                color: expiredDate.includes('-')
                  ? Palette.black
                  : Palette.color_ccc,
              }}>
              {expiredDate}
            </Text>
          </TouchableOpacity>
          <DateTimePickerModal
            isVisible={showDatePicker}
            mode="date"
            is24Hour={true}
            display={'spinner'}
            date={new Date()}
            onConfirm={(date: any) => {
              setShowDatePicker(false);
              setExpiredDate(moment(date).format('DD-MM-YYYY'));
            }}
            onCancel={() => {
              setShowDatePicker(false);
            }}
          />
        </View>
      </View>
    );
  };

  const addProductPrice = () => {
    setDataProductPrice([
      ...dataProductPrice,
      {
        name: '',
        comboSize: 0,
        price: 0,
        quantity: 0,
      },
    ]);
  };

  const addProduct = () => {
    setDataProduct([
      ...dataProduct,
      {
        image: '',
        name: '',
        note: '',
        isActive: false,
      },
    ]);
  };

  const validate_data = (
    image: string,
    name: string,
    note: string,
    discount: number,
    companyDiscount: number,
    expiredDate: number,
    description: string,
    dataProductPrice: {
      name: string;
      quantity: number;
      price: number;
      type: number;
    }[],
    dataProduct: {
      image: string;
      name: string;
      note: string;
      isActive: boolean;
    }[],
  ) => {
    let validate_fail = 0;

    return validate_fail == 0;
  };

  const updateCombo = useCallback(
    (
      productId,
      id,
      image,
      name,
      note,
      discount,
      companyDiscount,
      expiredDate,
      description,
      dataProductPrice,
      dataProduct,
    ) => {
      if (
        validate_data(
          image,
          name,
          note,
          discount,
          companyDiscount,
          expiredDate,
          description,
          dataProductPrice,
          dataProduct,
        )
      ) {
        dispatch(
          InfoAction.updateCombo(
            {
              productId: productId,
              id: id,
              image: image,
              name: name,
              note: note,
              discount: +discount,
              companyDiscount: +companyDiscount,
              expiredDate: new Date(
                moment(expiredDate, 'DD-MM-YYYY').format('YYYY-MM-DD'),
              ).getTime(),
              description: description,
              dataProductPrice: dataProductPrice,
              dataProduct: dataProduct,
            },
            {
              onBeginning: () => {
                dispatch(GlobalAction.setShowLoading({isLoading: true}));
              },
              onSuccess: (response: any) => {
                switch (response.errorCode) {
                  case code.success:
                    props.navigation.replace(ScreenMap.Home, {});
                    break;
                  case code.unauthorize:
                    props.navigation.navigate(ScreenMap.SignIn, {});
                    break;
                }
              },
              onFailure: (error: any) => {
                console.warn('error', error);
              },
              onFinish: () => {
                dispatch(GlobalAction.setShowLoading({isLoading: false}));
              },
            },
          ),
        );
      }
    },
    [dispatch, props.navigation],
  );

  const changePrice = (index: number, price: string) => {
    dataProductPrice[index].name = price;
    dataProductPrice[index].price = +price;
    setDataProductPrice([...dataProductPrice]);
  };

  const changeComboSize = (index: number, comboSize: number) => {
    dataProductPrice[index].comboSize = comboSize;
    setDataProductPrice([...dataProductPrice]);
  };

  const changeQuantity = (index: number, quantity: number) => {
    dataProductPrice[index].quantity = quantity;
    setDataProductPrice([...dataProductPrice]);
  };

  const deleteProductPrice = (index: number) => {
    dataProductPrice.splice(index, 1);
    setDataProductPrice([...dataProductPrice]);
  };

  const changeImageProduct = (index: number, image: string) => {
    dataProduct[index].image = image;
    setDataProduct([...dataProduct]);
  };

  const changeNameProduct = (index: number, name: string) => {
    dataProduct[index].name = name;
    setDataProduct([...dataProduct]);
  };

  const changeNoteProduct = (index: number, note: string) => {
    dataProduct[index].note = note;
    setDataProduct([...dataProduct]);
  };

  const toggleSelectProduct = (index: number) => {
    dataProduct[index].isActive = !dataProduct[index].isActive;
    setDataProduct([...dataProduct]);
  };

  const deleteProduct = (index: number) => {
    dataProduct.splice(index, 1);
    setDataProduct([...dataProduct]);
  };

  const uploadImage = (img: any) => {
    dispatch(
      DataAction.uploadImage(
        {formData: createFormData(img)},
        {
          onBeginning: () => {
            setLoading(true);
          },
          onSuccess: (response: any) => {
            setImage(response.data[0]);
          },
          onFailure: (error: any) => {
            console.warn(error);
          },
          onFinish: () => {
            setLoading(false);
          },
        },
      ),
    );
  };

  const open_gallery = () => {
    setChooseVisible(false);
    setTimeout(() => {
      ImagePicker.openPicker({
        width: 300,
        height: 400,
        cropping: true,
      })
        .then(img => {
          uploadImage(img);
        })
        .catch(err => {
          console.warn(err);
        });
    }, 200);
  };

  const open_camera = () => {
    setChooseVisible(false);
    setTimeout(() => {
      ImagePicker.openCamera({
        width: 300,
        height: 400,
        cropping: true,
      })
        .then(img => {
          uploadImage(img);
        })
        .catch(err => {
          console.warn(err);
        });
    }, 200);
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.container}>
        <View style={styles.groupImage}>
          <TouchableOpacity onPress={() => setChooseVisible(true)}>
            <View style={styles.containerImage}>
              {loading ? (
                <ActivityIndicator style={styles.loading} animating={true} />
              ) : null}
              <FastImage
                source={
                  image
                    ? {uri: env.API_ENDPOINT + image}
                    : media.add_image_product
                }
                style={image ? styles.image : styles.iconAdd}
                onLoadStart={() => (image ? setLoading(true) : null)}
                onLoadEnd={() => (image ? setLoading(false) : null)}
              />
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.infoHolder}>
          <Input
            label="Tên sản phẩm"
            placeHolder="Nhập tên sản phẩm"
            placeHolderColor={Palette.color_ccc}
            isRequired={true}
            maxLength={40}
            value={name}
            setValue={setName}
          />
          <Input
            label="Chú thích"
            placeHolder="Nhập chú thích"
            placeHolderColor={Palette.color_ccc}
            maxLength={40}
            value={note}
            setValue={setNote}
          />
          <Input
            label="Giảm giá cho người dùng(%)"
            placeHolder="Vui lòng nhập giảm giá"
            placeHolderColor={Palette.color_ccc}
            keyboardNumber={true}
            value={discount}
            setValue={setDiscount}
          />
          <Input
            label="Chiết khấu cho công ty(%)"
            placeHolder="Vui lòng nhập chiết khấu"
            placeHolderColor={Palette.color_ccc}
            keyboardNumber={true}
            value={companyDiscount}
            setValue={setCompanyDiscount}
          />
          {renderDatePicker()}
          <View style={styles.descripHolder}>
            <Text style={styles.titleDes}>{'Mô tả sản phẩm'}</Text>
            <TextInput
              multiline={true}
              value={description}
              maxLength={200}
              onChangeText={text => setDescription(text)}
            />
          </View>
        </View>
        <FlatList data={dataProductPrice} renderItem={renderProductPrice} />
        {dataProductPrice.length < 5 ? (
          <View style={styles.btnAddProductPrice}>
            <ButtonIcon
              title="Thêm"
              image={media.add_product}
              color={[Palette.color_ff0, Palette.color_ff4]}
              action={() => addProductPrice()}
            />
          </View>
        ) : null}
        <FlatList data={dataProduct} renderItem={renderProduct} />
        <View style={styles.btnAddProductPrice}>
          <ButtonIcon
            title="Thêm"
            image={media.add_product}
            color={[Palette.color_ff0, Palette.color_ff4]}
            action={() => addProduct()}
          />
        </View>
        <View style={styles.desHolder}>
          <ButtonSubmit
            title="Cập nhật"
            color={[Palette.color_ff0, Palette.color_ff4]}
            action={() =>
              updateCombo(
                productId,
                id,
                image,
                name,
                note,
                discount,
                companyDiscount,
                expiredDate,
                description,
                dataProductPrice,
                dataProduct,
              )
            }
          />
        </View>
        <PopUpImage
          visible={chooseVisible}
          setVisible={setChooseVisible}
          openCamera={() => open_camera()}
          openGallery={() => open_gallery()}
        />
      </ScrollView>
    </SafeAreaView>
  );
};
export default UpdateComboScreen;
