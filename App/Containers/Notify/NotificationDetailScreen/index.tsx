import {BaseScreenProps} from 'App/@types/screen-type';
import media from 'App/assets/media';
import {ScreenMap} from 'App/Config/NavigationConfig';
import moment from 'moment';
import React, {ReactElement} from 'react';
import {Image, SafeAreaView, ScrollView, Text, View} from 'react-native';
import styles from './styles';

const NotificationDetailScreen = (
  props: BaseScreenProps<ScreenMap.NotificationDetail>,
): ReactElement => {
  const item = props.route.params.item;
  return (
    <SafeAreaView style={styles.container}>
      <View style={[styles.infoHolder]}>
        <View style={styles.groupInfoAvatar}>
          <View style={styles.groupAvatar}>
            <Image source={media.avatar_default} style={styles.avatar} />
            <View style={styles.groupTitle}>
              <Text style={styles.txtTitle}>{item.title}</Text>
              <Text style={styles.txtDate}>
                {moment(item.time, 'x').format('HH:SS DD-MM-YYYY')}
              </Text>
            </View>
          </View>
        </View>
        <ScrollView style={styles.groupContent}>
          <Text style={styles.txtContent}>{item.payloadData.message}</Text>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};
export default NotificationDetailScreen;
