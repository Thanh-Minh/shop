import AsyncStorage from '@react-native-community/async-storage';
import {BaseScreenProps} from 'App/@types/screen-type';
import media from 'App/assets/media';
import {ScreenMap} from 'App/Config/NavigationConfig';
import {code} from 'App/Lib/codeHelpers';
import {Actions as DataAction} from 'App/Redux/DataRedux';
import moment from 'moment';
import React, {ReactElement, useCallback, useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
  Text,
  View,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useDispatch} from 'react-redux';
import styles from './styles';

const Item = ({item}: {item: NotifyItem}) => (
  <View style={[styles.infoHolder, item.readed == 1 ? styles.blur : null]}>
    <View style={styles.groupInfoAvatar}>
      <View style={styles.groupAvatar}>
        <Image source={media.avatar_default} style={styles.avatar} />
        <View style={styles.groupTitle}>
          <Text style={styles.txtTitle}>{item.title}</Text>
          <Text style={styles.txtDate}>
            {moment(item.time, 'x').format('HH:SS DD-MM-YYYY')}
          </Text>
        </View>
      </View>
    </View>
    <View style={styles.groupContent}>
      <Text style={styles.txtContent}>{item.payloadData.message}</Text>
    </View>
  </View>
);

const NotificationScreen = (
  props: BaseScreenProps<ScreenMap.Notification>,
): ReactElement => {
  const dispatch = useDispatch();
  const [data, setData] = useState([]);

  useEffect(() => {
    const unsubcribe = props.navigation.addListener('focus', () => {
      getListNoti(-1, 0, 20);
    });
    return unsubcribe;
  }, [props.navigation]);

  const getListNoti = useCallback(
    async (
      typeNoti: number,
      page: number,
      pageSize: number,
      endTime?: number,
      startTime?: number,
    ) => {
      let fcmToken = await AsyncStorage.getItem('fcmToken');
      dispatch(
        DataAction.getListNotification(
          {
            deviceId: fcmToken,
            typeNoti: typeNoti,
            endTime: endTime,
            page: page,
            pageSize: pageSize,
            startTime: startTime,
          },
          {
            onBeginning: () => {},
            onSuccess: (response: any) => {
              switch (response.errorCode) {
                case code.success:
                  setData(response.data.data);
                  break;
                case code.unauthorize:
                  props.navigation.navigate(ScreenMap.SignIn, {});
                  break;
              }
            },
            onFailure: (error: any) => {
              console.log(error);
            },
            onFinish: () => {},
          },
        ),
      );
    },
    [data],
  );

  const renderItem = ({item}: {item: NotifyItem}) => {
    if (item.readed == 1) {
      return <Item item={item} />;
    } else {
      return (
        <TouchableOpacity
          onPress={() => {
            props.navigation.navigate('BottomTab', {
              screen: 'Notify',
              params: {
                screen: ScreenMap.NotificationDetail,
                params: {item: item},
              },
            });
          }}>
          <Item item={item} />
        </TouchableOpacity>
      );
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.containerScroll}>
        <FlatList
          data={data}
          renderItem={renderItem}
          scrollEnabled={false}
          keyExtractor={item => item.id}
        />
      </ScrollView>
    </SafeAreaView>
  );
};
export default NotificationScreen;
