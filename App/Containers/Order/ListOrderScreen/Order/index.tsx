import {useNavigation} from '@react-navigation/native';
import {ScreenMap} from 'App/Config/NavigationConfig';
import {numberWithDots} from 'App/Lib/convertDataHelper';
import React, {ReactElement} from 'react';
import {FlatList, SafeAreaView, ScrollView, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import styles from './styles';

const Item = ({item}: {item: OrderItem}) => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      style={styles.itemHolder}
      onPress={() => {
        navigation.navigate('BottomTab', {
          screen: 'Order',
          params: {
            screen: ScreenMap.OrderDetail,
            params: {item: item},
          },
        });
      }}>
      <View style={styles.contentHolder}>
        <Text style={[styles.textContent, styles.titleContent]}>
          {'Đơn hàng: ' + item.orderCode}
        </Text>
        <Text style={styles.textContent}>
          {item.productName +
            ' - ' +
            numberWithDots(+item.detailProdName) +
            ' ' +
            item.unitName +
            ' - ' +
            item.quantity}
        </Text>
        <Text style={styles.textContent}>
          {'Địa chỉ: ' + item.deliveryAddress}
        </Text>
        {item.deliveryNote ? (
          <Text style={[styles.textContent, styles.note]}>
            {`Lưu ý: ${item.deliveryNote}`}
          </Text>
        ) : null}
      </View>
    </TouchableOpacity>
  );
};

const Order = ({data}: {data: any}): ReactElement => {
  const renderItem = ({item}: {item: OrderItem}) => {
    return <Item item={item} />;
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.containerScroll}>
        <FlatList data={data} renderItem={renderItem} />
      </ScrollView>
    </SafeAreaView>
  );
};
export default Order;
