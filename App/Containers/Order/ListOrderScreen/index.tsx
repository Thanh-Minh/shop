import {BaseScreenProps} from 'App/@types/screen-type';
import {ScreenMap} from 'App/Config/NavigationConfig';
import {screenWidth} from 'App/Config/ScreenConfig';
import {code} from 'App/Lib/codeHelpers';
import {Actions as DataAction} from 'App/Redux/DataRedux';
import {Actions as GlobalAction} from 'App/Redux/GlobalRedux';
import {Palette} from 'App/Theme/Palette';
import React, {ReactElement, useEffect, useState} from 'react';
import {Text, View} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {TabBar, TabView} from 'react-native-tab-view';
import {useDispatch} from 'react-redux';
import Order from './Order';
import OrderComplete from './OrderComplete';
import OrderOnShip from './OrderOnShip';
import OrderPrepare from './OrderPrepare';
import styles from './styles';

const initialLayout = {width: screenWidth};

const ListOrderScreen = (
  props: BaseScreenProps<ScreenMap.ListOrder>,
): ReactElement => {
  const dispatch = useDispatch();
  const [index, setIndex] = useState(0);
  const [routes] = useState([
    {key: 'order', title: 'Đơn mới'},
    {key: 'orderprepare', title: 'Chờ lấy'},
    {key: 'orderonship', title: 'Đang giao'},
    {key: 'ordercomplete', title: 'Đã giao'},
  ]);
  const [data, setData] = useState([]);

  useEffect(() => {
    const unsubcribe = props.navigation.addListener('focus', () => {
      const newOrder = 1;
      setIndex(0);
      getList(newOrder);
    });
    return unsubcribe;
  }, [dispatch, props.navigation]);

  const getList = (index: number) => {
    dispatch(
      DataAction.getListOrder(
        {status: index},
        {
          onBeginning: () => {
            dispatch(GlobalAction.setShowLoading({isLoading: true}));
          },
          onSuccess: (response: any) => {
            switch (response.errorCode) {
              case code.success:
                setData(response.data.data);
                break;
              case code.unauthorize:
                props.navigation.navigate(ScreenMap.SignIn, {});
                break;
            }
          },
          onFailure: (error: any) => {
            console.log(error);
          },
          onFinish: () => {
            dispatch(GlobalAction.setShowLoading({isLoading: false}));
          },
        },
      ),
    );
  };

  const onChangetab = (index: number) => {
    setIndex(index);
    setData([]);
    getList(index + 1);
  };

  const renderScene = ({route}: {route: any}) => {
    switch (route.key) {
      case 'order':
        return <Order data={data} />;
      case 'orderprepare':
        return <OrderPrepare data={data} />;
      case 'orderonship':
        return <OrderOnShip data={data} />;
      case 'ordercomplete':
        return <OrderComplete data={data} />;
      default:
        return null;
    }
  };

  const renderTabBar = (props: any) => (
    <TabBar
      {...props}
      getLabelText={({route}) => route.title}
      inactiveColor={Palette.black}
      activeColor={Palette.color_e91}
      indicatorStyle={{backgroundColor: Palette.white}}
      style={{backgroundColor: Palette.white}}
      renderLabel={({route, focused, color}) => (
        <Text style={{color, fontSize: RFValue(10, 580)}}>{route.title}</Text>
      )}
    />
  );

  return (
    <View style={styles.container}>
      <TabView
        renderTabBar={renderTabBar}
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={index => onChangetab(index)}
        initialLayout={initialLayout}
      />
    </View>
  );
};
export default ListOrderScreen;
