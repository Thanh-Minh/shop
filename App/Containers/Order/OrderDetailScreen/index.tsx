import {BaseScreenProps} from 'App/@types/screen-type';
import ButtonSubmit from 'App/Components/Form/ButtonSubmit';
import {ScreenMap} from 'App/Config/NavigationConfig';
import {code} from 'App/Lib/codeHelpers';
import {Actions as DataAction} from 'App/Redux/DataRedux';
import {Palette} from 'App/Theme/Palette';
import React, {ReactElement} from 'react';
import {SafeAreaView, ScrollView, Text, View} from 'react-native';
import {useDispatch} from 'react-redux';
import styles from './styles';

const OrderDetailScreen = (
  props: BaseScreenProps<ScreenMap.OrderDetail>,
): ReactElement => {
  const dispatch = useDispatch();
  const order_info = props.route.params.item;

  const takeCompleteOrder = () => {
    dispatch(
      DataAction.completeOrder(
        {cartId: order_info.id},
        {
          onBeginning: () => {},
          onSuccess: (response: any) => {
            switch (response.errorCode) {
              case code.success:
                props.navigation.goBack();
                break;
              case code.unauthorize:
                props.navigation.navigate(ScreenMap.SignIn, {});
                break;
            }
          },
          onFailure: (error: any) => {
            console.log(error);
          },
          onFinish: () => {},
        },
      ),
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.container}>
        <View style={styles.itemHolder}>
          <Text style={[styles.textContent, styles.titleContent]}>
            {'Đơn hàng: ' + order_info.orderCode}
          </Text>
          <Text style={styles.textContent}>{order_info.productName}</Text>
          <Text style={styles.textContent}>
            {'Số lượng ' + order_info.quantity}
          </Text>
          <Text style={styles.textContent}>
            {'Địa chỉ: ' + order_info.deliveryAddress}
          </Text>
          {order_info.deliveryNote ? (
            <Text style={[styles.textContent, styles.note]}>
              {`Lưu ý: ${order_info.deliveryNote}`}
            </Text>
          ) : null}
        </View>
        <View style={styles.statusHolder}>
          <Text style={[styles.textContent, styles.titleContent]}>
            {'Tình trạng đơn hàng'}
          </Text>
          <View style={styles.statusOrder}>
            <Text style={[styles.textContent, styles.alignTextCenterVertical]}>
              {'Đã đóng gói, sẵn sàng giao'}
            </Text>
            <View style={styles.btnStatus}>
              <ButtonSubmit
                title="Xác nhận"
                color={[Palette.color_ff0, Palette.color_ff4]}
                isSmall={true}
                action={() => takeCompleteOrder()}
              />
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
export default OrderDetailScreen;
