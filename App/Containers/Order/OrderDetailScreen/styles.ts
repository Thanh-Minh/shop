import {Palette} from 'App/Theme/Palette';
import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Palette.color_fbf,
  },
  itemHolder: {
    flex: 1,
    marginTop: 10,
    padding: 10,
    paddingBottom: 20,
    backgroundColor: Palette.white,
    shadowColor: Palette.black,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  statusHolder: {
    flex: 1,
    marginTop: 10,
    padding: 10,
    paddingBottom: 20,
    backgroundColor: Palette.white,
    shadowColor: Palette.black,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
    marginBottom: 10,
  },
  textContent: {
    fontSize: RFValue(10, 580),
    marginHorizontal: 8,
    marginTop: 7,
    color: Palette.color_3a3,
  },
  note: {
    color: Palette.color_ff4,
  },
  titleContent: {
    fontSize: RFValue(12, 580),
    fontWeight: 'bold',
  },
  statusOrder: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
    marginTop: 10,
  },
  alignTextCenterVertical: {
    alignSelf: 'center',
  },
  btnStatus: {
    width: 130,
    alignContent: 'center',
  },
});
export default styles;
