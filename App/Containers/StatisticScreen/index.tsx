import media from 'App/assets/media';
import ButtonSubmit from 'App/Components/Form/ButtonSubmit';
import TextLink from 'App/Components/Form/TextLink';
import env from 'App/Config/Enviroment/env';
import {ScreenMap} from 'App/Config/NavigationConfig';
import {code} from 'App/Lib/codeHelpers';
import {numberWithDots} from 'App/Lib/convertDataHelper';
import {Actions as DataAction} from 'App/Redux/DataRedux';
import {Palette} from 'App/Theme/Palette';
import moment from 'moment';
import React, {ReactElement, useCallback, useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
  Text,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {RFValue} from 'react-native-responsive-fontsize';
import {useDispatch} from 'react-redux';
import styles from './styles';

const Item = ({
  item,
  index,
}: {
  item: {title: string; number: number; color: string};
  index: number;
}) => (
  <View style={[styles.itemHolder, {backgroundColor: item.color}]}>
    <Text style={styles.txtTitle}>{item.title}</Text>
    <Text style={styles.txtNumber}>
      {index == 0 ? numberWithDots(item.number) : item.number}
    </Text>
  </View>
);

const ItemProduct = ({
  item,
}: {
  item: {
    id: number;
    image: string;
    name: string;
    price: number;
    remainQuantity: number;
    soldAmount: number;
    unitName: string;
  };
}) => (
  <View style={styles.itemProductHolder}>
    <FastImage
      source={{uri: env.API_ENDPOINT + item.image}}
      style={styles.imgProduct}
    />
    <Text style={styles.txtNameProduct}>{item.name}</Text>
    <Text style={styles.txtPriceProduct}>
      {numberWithDots(item.price) + ' đ/kg'}
    </Text>
    <Text style={styles.txtAmountProduct}>{'Đã bán: ' + item.soldAmount}</Text>
  </View>
);

const StatisticScreen = (props: any): ReactElement => {
  const dispatch = useDispatch();
  const [rate, setRate] = useState(0);
  const [chooseDateIndex, setChooseDateIndex] = useState(0);
  const [dateStart, setDateStart] = useState(moment().format('DD/MM/YYYY'));
  const [dateEnd, setDateEnd] = useState(moment().format('DD/MM/YYYY'));
  const [balanceAccount, setBalanceAccount] = useState(0);
  const [data, setData] = useState([
    {
      title: 'Doanh thu',
      number: 0,
      color: '#FF8B00',
    },
    {
      title: 'Sản phẩm bán',
      number: 0,
      color: '#03B22A',
    },
    {
      title: 'Đơn đặt',
      number: 0,
      color: '#6A6ACC',
    },
    {
      title: 'Đơn huỷ',
      number: 0,
      color: '#D14261',
    },
  ]);
  const [totalProduct, setTotalProduct] = useState(0);
  const [commissionForCompany, setCommissionForCompany] = useState(0);
  const [productOnSale, setProductOnSale] = useState(0);
  const [dataBestSell, setDataBestSell] = useState();
  const [dataPoorSell, setDataPoorSell] = useState();

  useEffect(() => {
    const unsubcribe = props.navigation.addListener('focus', () => {
      dispatch(
        DataAction.getListProductStatistic(
          {},
          {
            onBeginning: () => {},
            onSuccess: (response: any) => {
              switch (response.errorCode) {
                case code.success:
                  const dataList = response.data;
                  dataList.bestSellingProducts.length > 0
                    ? setDataBestSell(dataList.bestSellingProducts)
                    : setDataBestSell(dataList.sellingProducts);

                  dataList.poorSellingProducts.length > 0
                    ? setDataPoorSell(dataList.poorSellingProducts)
                    : setDataPoorSell(dataList.sellingProducts);
                  break;
                case code.unauthorize:
                  props.navigation.navigate(ScreenMap.SignIn, {});
                  break;
              }
            },
            onFailure: (error: any) => {
              console.error(error);
            },
            onFinish: () => {},
          },
        ),
      );
    });
    return unsubcribe;
  }, [dispatch, props.navigation]);

  useEffect(() => {
    getDataStatistic(
      new Date(
        +dateStart.split('/')[2],
        +dateStart.split('/')[1] - 1,
        +dateStart.split('/')[0],
      ).setHours(0, 0, 0),
      new Date(
        +dateEnd.split('/')[2],
        +dateEnd.split('/')[1] - 1,
        +dateEnd.split('/')[0],
      ).setHours(23, 59, 0),
    );
  }, [dateStart, dateEnd]);

  const getDataStatistic = useCallback((dateStart: number, dateEnd: number) => {
    dispatch(
      DataAction.getDataStatistic(
        {startTime: dateStart, endTime: dateEnd},
        {
          onBeginning: () => {},
          onSuccess: (response: any) => {
            const info = response.data.data;
            setBalanceAccount(info.balanceAccount);
            data[0].number = info.revenue;
            data[1].number = info.totalSoldProduct;
            data[2].number = info.totalOrder;
            data[3].number = info.cancelOrder;
            setData([...data]);
            setCommissionForCompany(info.commissionForCompany);
            setTotalProduct(info.totalProduct);
            setProductOnSale(info.totalProductOnSale);
          },
          onFailure: (error: any) => {
            console.warn(error);
          },
          onFinish: () => {},
        },
      ),
    );
  }, []);

  const renderItem = ({
    item,
    index,
  }: {
    item: {title: string; number: number; color: string};
    index: number;
  }) => {
    return <Item item={item} index={index} />;
  };

  const renderProduct = ({
    item,
  }: {
    item: {
      id: number;
      image: string;
      name: string;
      price: number;
      remainQuantity: number;
      soldAmount: number;
      unitName: string;
    };
  }) => {
    return <ItemProduct item={item} />;
  };

  const setIndexButton = useCallback(index => {
    setChooseDateIndex(index);
    switch (index) {
      case 0:
        setDateStart(moment().format('DD/MM/YYYY'));
        setDateEnd(moment().format('DD/MM/YYYY'));
        break;
      case 1:
        setDateStart(
          moment()
            .startOf('week')
            .format('DD/MM/YYYY'),
        );
        setDateEnd(moment().format('DD/MM/YYYY'));
        break;
      case 2:
        setDateStart(
          moment()
            .startOf('month')
            .format('DD/MM/YYYY'),
        );
        setDateEnd(moment().format('DD/MM/YYYY'));
        break;
    }
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.container}>
        <View style={styles.amountHolder}>
          <Text style={styles.txtUser}>{'Tài khoản'}</Text>
          <Text style={styles.txtUserAmount}>
            {numberWithDots(balanceAccount) + ' đ'}
          </Text>
        </View>
        <View style={styles.dateHolder}>
          <View style={styles.groupDateDetail}>
            <Text style={styles.dateHolderTitle}>{'Thống kê'}</Text>
            <View style={styles.groupPickDate}>
              <Text style={styles.dateRange}>
                {dateStart === dateEnd
                  ? dateStart
                  : dateStart + ' - ' + dateEnd}
              </Text>
              <Image
                style={styles.dateStatisticIcon}
                source={media.date_statistic}
              />
            </View>
          </View>
          <View style={styles.dateButtonHolder}>
            <View style={styles.btnHolder}>
              <ButtonSubmit
                title="Hôm nay"
                color={
                  chooseDateIndex == 0
                    ? [Palette.color_f66, Palette.color_f66]
                    : [Palette.color_9b9, Palette.color_9b9]
                }
                isSmall={true}
                action={() => {
                  setIndexButton(0);
                }}
              />
            </View>
            <View style={styles.btnHolder}>
              <ButtonSubmit
                title="Tuần"
                color={
                  chooseDateIndex == 1
                    ? [Palette.color_f66, Palette.color_f66]
                    : [Palette.color_9b9, Palette.color_9b9]
                }
                isSmall={true}
                action={() => {
                  setIndexButton(1);
                }}
              />
            </View>
            <View style={styles.btnHolder}>
              <ButtonSubmit
                title="Tháng"
                color={
                  chooseDateIndex == 2
                    ? [Palette.color_f66, Palette.color_f66]
                    : [Palette.color_9b9, Palette.color_9b9]
                }
                isSmall={true}
                action={() => {
                  setIndexButton(2);
                }}
              />
            </View>
          </View>
        </View>
        <View style={styles.groupItemHolder}>
          <FlatList data={data} numColumns={2} renderItem={renderItem} />
        </View>
        <View style={styles.groupInfo}>
          <View style={styles.groupInfoContent}>
            <Text style={styles.txtTitleContent}>{'Số chiết khấu:'}</Text>
            <Text style={styles.txtContent}>
              {numberWithDots(commissionForCompany) + ' đ'}
            </Text>
          </View>
          <View style={styles.groupInfoContent}>
            <Text style={styles.txtTitleContent}>{'Tổng sản phẩm:'}</Text>
            <Text style={styles.txtContent}>{totalProduct}</Text>
          </View>
          <View style={styles.groupInfoContent}>
            <Text style={styles.txtTitleContent}>{'Sản phẩm đang bán:'}</Text>
            <Text style={styles.txtContent}>{productOnSale}</Text>
          </View>
        </View>
        <View style={styles.groupRate}>
          <Text style={styles.txtTitleContent}>{'Đánh giá cửa hàng'}</Text>
          <View style={styles.groupStar}>
            {[...Array(5)].map((el, i) => (
              <Image
                style={styles.starImage}
                source={i <= rate - 1 ? media.star : media.star_inactive}
              />
            ))}
          </View>
          <TextLink
            textTitle="(2 đánh giá/ hôm nay) "
            textLink="xem chi tiết..."
            fontSize={RFValue(10, 580)}
            color={Palette.color_e40}
          />
        </View>
        <View style={styles.groupBestSell}>
          <Text style={styles.txtTitleBestSell}>{'Mặt hàng bán chạy'}</Text>
          <FlatList
            style={styles.marTop10}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            data={dataBestSell}
            renderItem={renderProduct}
          />
        </View>
        <View style={styles.groupInventory}>
          <Text style={styles.txtTitleInventory}>{'Mặt hàng bán chậm'}</Text>
          <FlatList
            style={styles.marTop10}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            data={dataPoorSell}
            renderItem={renderProduct}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
export default StatisticScreen;
