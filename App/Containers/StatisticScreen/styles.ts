import {Palette} from 'App/Theme/Palette';
import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Palette.color_fbf,
  },
  amountHolder: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
    margin: 10,
    backgroundColor: Palette.color_4a8,
    borderRadius: 6,
  },
  txtUser: {
    flex: 1,
    color: Palette.white,
    fontWeight: 'bold',
    fontSize: RFValue(15, 580),
  },
  txtUserAmount: {
    flex: 1,
    color: Palette.white,
    fontWeight: 'bold',
    fontSize: RFValue(15, 580),
    textAlign: 'right',
  },
  groupDateDetail: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  dateRange: {
    fontSize: RFValue(11, 580),
    fontWeight: '400',
    alignSelf: 'center',
  },
  groupPickDate: {
    flexDirection: 'row',
  },
  dateStatisticIcon: {
    width: 21,
    height: 21,
    resizeMode: 'contain',
    marginHorizontal: 15,
    alignSelf: 'center',
  },
  dateHolder: {
    flex: 1,
    margin: 10,
    marginLeft: 20,
    marginRight: 10,
  },
  dateHolderTitle: {
    fontWeight: '600',
    fontSize: RFValue(13, 580),
  },
  dateButtonHolder: {
    flex: 1,
    marginHorizontal: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 15,
  },
  btnHolder: {
    flex: 1,
    width: '30%',
    marginHorizontal: 15,
  },
  groupItemHolder: {
    flex: 1,
    marginTop: 5,
  },
  itemHolder: {
    flex: 1,
    margin: 10,
    padding: 15,
    borderRadius: 6,
    minHeight: 90,
    justifyContent: 'center',
    alignContent: 'center',
  },
  txtTitle: {
    flex: 1,
    fontSize: RFValue(11, 580),
    fontWeight: '500',
    color: Palette.white,
  },
  txtNumber: {
    flex: 1,
    fontSize: RFValue(13, 580),
    fontWeight: '500',
    color: Palette.white,
    marginTop: 10,
  },
  groupInfo: {
    flex: 1,
    margin: 10,
    padding: 20,
    backgroundColor: Palette.white,
    borderRadius: 6,
  },
  groupInfoContent: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 5,
  },
  txtTitleContent: {
    fontSize: RFValue(11, 580),
    fontWeight: '300',
  },
  txtContent: {
    fontSize: RFValue(12, 580),
    fontWeight: '400',
  },
  groupRate: {
    flex: 1,
    margin: 10,
  },
  starImage: {
    width: 40,
    height: 40,
    resizeMode: 'cover',
    marginHorizontal: 5,
  },
  groupStar: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 20,
  },
  groupBestSell: {
    flex: 1,
    margin: 10,
  },
  txtTitleBestSell: {
    fontSize: RFValue(11, 580),
    fontWeight: 'bold',
  },
  groupInventory: {
    flex: 1,
    margin: 10,
  },
  txtTitleInventory: {
    fontSize: RFValue(11, 580),
    fontWeight: 'bold',
  },
  itemProductHolder: {
    flex: 0.5,
    width: 130,
    height: '100%',
    margin: 7,
    padding: 10,
    paddingTop: 20,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Palette.white,
    shadowColor: Palette.black,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  imgProduct: {
    flex: 1,
    width: '90%',
    height: 70,
    resizeMode: 'contain',
  },
  txtNameProduct: {
    fontSize: RFValue(9, 580),
    color: Palette.color_342,
    marginTop: 20,
  },
  txtPriceProduct: {
    fontSize: RFValue(12, 580),
    fontWeight: '600',
    color: Palette.color_ff1,
    marginTop: 5,
  },
  txtAmountProduct: {
    fontSize: RFValue(9, 580),
    color: Palette.color_342,
    marginTop: 5,
  },
  marTop10: {
    marginTop: 10,
  },
});
export default styles;
