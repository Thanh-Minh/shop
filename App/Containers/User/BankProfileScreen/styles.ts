import {Palette} from 'App/Theme/Palette';
import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Palette.color_fbf,
  },
  bankHolder: {
    flex: 1,
    marginTop: 5,
    marginBottom: 5,
    padding: 10,
    paddingBottom: 20,
    backgroundColor: Palette.white,
    shadowColor: Palette.black,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  groupTitleBank: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 5,
  },
  txtTitleBank: {
    fontSize: RFValue(13, 580),
    fontWeight: 'bold',
    marginTop: 10,
  },
  groupUserInfo: {
    flex: 1,
    marginTop: 25,
  },
  label: {
    color: Palette.color_545,
    fontSize: RFValue(10, 580),
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  otpInput: {
    maxHeight: 100,
    width: '70%',
    alignSelf: 'center',
  },
  underlineStyleBase: {
    width: 40,
    height: 50,
    borderWidth: 0,
    borderBottomWidth: 4,
    fontSize: RFValue(16, 580),
    fontWeight: '500',
    color: Palette.color_1e2,
  },
  underlineStyleHighLighted: {
    borderColor: Palette.color_ffc,
  },
  otpTitle: {
    color: Palette.color_6b6,
    fontSize: RFValue(10, 580),
    textAlign: 'center',
  },
  overlay: {
    flex: 1,
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    backgroundColor: Palette.black,
    opacity: 0.6,
    zIndex: 0,
  },
});
export default styles;
