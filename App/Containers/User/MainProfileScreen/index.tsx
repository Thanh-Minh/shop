import AsyncStorage from '@react-native-community/async-storage';
import {BaseScreenProps} from 'App/@types/screen-type';
import media from 'App/assets/media';
import ItemMenu from 'App/Components/Form/ItemMenu';
import {ScreenMap} from 'App/Config/NavigationConfig';
import {Actions as AuthActions} from 'App/Redux/AuthRedux';
import {Actions as GlobalAction} from 'App/Redux/GlobalRedux';
import {selectUserData} from 'App/Selector/AuthSelector';
import React, {ReactElement, useCallback, useEffect, useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import styles from './styles';

const MainProfileScreen = (
  props: BaseScreenProps<ScreenMap.MainProfile>,
): ReactElement => {
  const dispatch = useDispatch();
  const userInfo = useSelector(selectUserData);
  const [info, setInfo] = useState({
    fullName: '',
    userName: '',
  });

  useEffect(() => {
    const unsubcribe = props.navigation.addListener('focus', () => {
      getDataUser();
    });
    return unsubcribe;
  }, [props.navigation]);

  const getDataUser = () => {
    setInfo({
      fullName: userInfo.fullName,
      userName: userInfo.userName,
    });
    AsyncStorage.getItem('userInfo').then(info => {
      if (info) {
        const dataInfo = JSON.parse(info);
        setInfo({
          fullName: dataInfo.fullName,
          userName: dataInfo.userName,
        });
      }
    });
  };

  const logOut = useCallback(() => {
    dispatch(
      AuthActions.resetAuth(
        {},
        {
          onBeginning: () => {
            dispatch(GlobalAction.setShowLoading({isLoading: true}));
          },
          onSuccess: () => {
            AsyncStorage.clear().then(_clear => {
              props.navigation.navigate('Auth', {
                screen: ScreenMap.SignIn,
              });
            });
          },
          onFailure: (error: any) => {
            console.warn('error', error);
          },
          onFinish: () => {
            dispatch(GlobalAction.setShowLoading({isLoading: false}));
          },
        },
      ),
    );
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.container}>
        <View style={styles.infoHolder}>
          <TouchableOpacity
            style={styles.groupInfoAvatar}
            onPress={() => {
              props.navigation.navigate(ScreenMap.Profile, {});
            }}>
            <View style={styles.groupAvatar}>
              <View>
                <Image source={media.avatar_default} style={styles.avatar} />
              </View>
              <View style={styles.groupUserName}>
                <Text style={styles.txtUserName}>{info.fullName}</Text>
                <Text style={styles.txtUserCode}>{info.userName}</Text>
              </View>
            </View>
            <View>
              <Image
                source={media.chevron_right_black}
                style={styles.iconProfileDirect}
              />
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.menuHolder}>
          <ItemMenu
            label={'Thông tin shop'}
            image={media.shop_info}
            action={() => {
              props.navigation.navigate(ScreenMap.ShopProfile, {});
            }}
          />
          <ItemMenu
            label={'Ngân hàng'}
            image={media.bank}
            action={() => {
              props.navigation.navigate(ScreenMap.BankProfile, {});
            }}
          />
          <ItemMenu label={'Liên hệ'} image={media.support} />
          <ItemMenu
            label={'Đăng xuất'}
            image={media.logout}
            action={() => logOut()}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
export default MainProfileScreen;
