import {Palette} from 'App/Theme/Palette';
import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Palette.color_fbf,
  },
  infoHolder: {
    flex: 1,
    marginTop: 10,
    marginBottom: 5,
    padding: 15,
    paddingBottom: 20,
    backgroundColor: Palette.white,
    shadowColor: Palette.black,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  groupInfoAvatar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  groupAvatar: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  menuHolder: {
    flex: 1,
    marginTop: 5,
    marginBottom: 5,
    padding: 10,
    paddingBottom: 20,
    backgroundColor: Palette.white,
    shadowColor: Palette.black,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  avatar: {
    width: 60,
    height: 60,
    borderRadius: 30,
    resizeMode: 'contain',
    marginHorizontal: 5,
  },
  groupUserName: {
    marginLeft: 15,
  },
  txtUserName: {
    fontSize: RFValue(13, 580),
    fontWeight: 'bold',
    marginBottom: 7,
  },
  txtUserCode: {
    fontSize: RFValue(11, 580),
    color: Palette.color_8a8,
  },
  groupTitleBank: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 5,
  },
  txtTitleBank: {
    fontSize: RFValue(13, 580),
    fontWeight: 'bold',
    marginTop: 10,
  },
  groupUserInfo: {
    flex: 1,
    marginTop: 25,
  },
  txtLabelInfo: {
    fontSize: RFValue(10, 580),
    color: Palette.color_555,
    marginTop: 10,
  },
  txtInfo: {
    fontSize: RFValue(10, 580),
    color: Palette.color_1f1,
    marginTop: 10,
  },
  groupInfo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 5,
  },
  iconProfileDirect: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
  },
});
export default styles;
