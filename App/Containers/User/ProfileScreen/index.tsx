import AsyncStorage from '@react-native-community/async-storage';
import {BaseScreenProps} from 'App/@types/screen-type';
import media from 'App/assets/media';
import {ScreenMap} from 'App/Config/NavigationConfig';
import {code} from 'App/Lib/codeHelpers';
import {Actions as GlobalAction} from 'App/Redux/GlobalRedux';
import {Actions as InfoAction} from 'App/Redux/InfoRedux';
import {selectUserData} from 'App/Selector/AuthSelector';
import moment from 'moment';
import React, {ReactElement, useEffect, useState} from 'react';
import {Image, SafeAreaView, ScrollView, Text, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import styles from './styles';

const ProfileScreen = (
  props: BaseScreenProps<ScreenMap.Profile>,
): ReactElement => {
  const dispatch = useDispatch();
  const userInfo = useSelector(selectUserData);
  const [ownerShopInfo, setOwnerShopInfo] = useState({
    birthday: 0,
    gender: 0,
    address: '',
  });

  const [info, setInfo] = useState({
    fullName: '',
    userName: '',
  });

  useEffect(() => {
    const unsubcribe = props.navigation.addListener('focus', () => {
      getDataUser();
    });
    return unsubcribe;
  }, [props.navigation]);

  const getDataUser = () => {
    setInfo({
      fullName: userInfo.fullName,
      userName: userInfo.userName,
    });
    AsyncStorage.getItem('userInfo').then(info => {
      if (info) {
        const dataInfo = JSON.parse(info);
        setInfo({
          fullName: dataInfo.fullName,
          userName: dataInfo.userName,
        });
      }
    });
  };

  useEffect(() => {
    const unsubcribe = props.navigation.addListener('focus', () => {
      dispatch(
        InfoAction.getInfoOwnerShop(
          {},
          {
            onBeginning: () => {
              dispatch(GlobalAction.setShowLoading({isLoading: true}));
            },
            onSuccess: (response: any) => {
              switch (response.errorCode) {
                case code.success:
                  const info = response.data;
                  setOwnerShopInfo({
                    birthday: info.birthday,
                    gender: info.gender,
                    address: info.address.name,
                  });
                  break;
                case code.unauthorize:
                  props.navigation.navigate(ScreenMap.SignIn, {});
                  break;
              }
            },
            onFailure: () => {},
            onFinish: () => {
              dispatch(GlobalAction.setShowLoading({isLoading: true}));
            },
          },
        ),
      );
    });
    return unsubcribe;
  }, [dispatch, props.navigation]);

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.container}>
        <View style={styles.infoHolder}>
          <View style={styles.groupInfoAvatar}>
            <View style={styles.groupAvatar}>
              <View>
                <Image source={media.avatar_default} style={styles.avatar} />
              </View>
              <View style={styles.groupUserName}>
                <Text style={styles.txtUserName}>{info.fullName}</Text>
                <Text style={styles.txtUserCode}>{info.userName}</Text>
              </View>
            </View>
          </View>
          <View style={styles.groupUserInfo}>
            <View style={styles.groupInfo}>
              <Text style={styles.txtLabelInfo}>{'Ngày Sinh'}</Text>
              <Text style={styles.txtInfo}>
                {moment(ownerShopInfo.birthday, 'x').format('DD/MM/YYYY')}
              </Text>
            </View>
            <View style={styles.groupInfo}>
              <Text style={styles.txtLabelInfo}>{'Giới tính'}</Text>
              <Text style={styles.txtInfo}>
                {ownerShopInfo.gender == 1 ? 'Nam' : 'Nữ'}
              </Text>
            </View>
            <View style={styles.groupInfo}>
              <Text style={styles.txtLabelInfo}>{'Địa chỉ'}</Text>
              <Text style={styles.txtInfo}>{ownerShopInfo.address}</Text>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
export default ProfileScreen;
