import {Palette} from 'App/Theme/Palette';
import {Platform, StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Palette.color_fbf,
  },
  infoHolder: {
    flex: 1,
    marginTop: 10,
    marginBottom: 5,
    padding: 15,
    paddingBottom: 20,
    backgroundColor: Palette.white,
    shadowColor: Palette.black,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  groupInfoAvatar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  groupAvatar: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  bankHolder: {
    flex: 1,
    marginTop: 5,
    marginBottom: 5,
    padding: 10,
    paddingBottom: 20,
    backgroundColor: Palette.white,
    shadowColor: Palette.black,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  avatar: {
    width: 60,
    height: 60,
    borderRadius: 30,
    resizeMode: 'contain',
    marginHorizontal: 5,
  },
  groupUserName: {
    marginLeft: 15,
  },
  txtUserName: {
    fontSize: RFValue(13, 580),
    fontWeight: 'bold',
    marginBottom: 7,
  },
  txtUserCode: {
    fontSize: RFValue(11, 580),
    color: Palette.black,
  },
  groupTitleBank: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 5,
  },
  txtTitleBank: {
    fontSize: RFValue(13, 580),
    fontWeight: 'bold',
    marginTop: 10,
  },
  groupUserInfo: {
    flex: 1,
    marginTop: 25,
  },
  txtLabelInfo: {
    fontSize: RFValue(10, 580),
    color: Palette.color_555,
    marginTop: 10,
  },
  txtInfo: {
    fontSize: RFValue(10, 580),
    color: Palette.color_1f1,
    marginTop: 10,
  },
  groupInfo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 5,
  },
  overlay: {
    flex: 1,
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    backgroundColor: Palette.black,
    opacity: 0.6,
    zIndex: 0,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginBottom: Platform.OS === 'ios' ? 30 : 10,
  },
  modalContent: {
    width: '90%',
    height: 90,
    backgroundColor: Palette.white,
    borderRadius: 10,
    borderWidth: 0.6,
    borderColor: Palette.color_ccc,
    shadowColor: Palette.black,
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.3,
    shadowRadius: 2,
  },
  modalCancel: {
    width: '90%',
    height: 50,
    marginTop: 10,
    backgroundColor: Palette.white,
    borderRadius: 10,
    borderWidth: 0.6,
    borderColor: Palette.color_ccc,
    shadowColor: Palette.black,
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.3,
    shadowRadius: 2,
  },
  openButton: {
    borderTopWidth: 0.5,
    borderTopColor: Palette.color_ccc,
    padding: 10,
    zIndex: 1,
  },
  openButtonNoLine: {
    padding: 10,
    zIndex: 1,
  },
  textStyle: {
    fontSize: RFValue(15, 580),
    textAlign: 'center',
  },
  option: {
    color: Palette.color_3c8,
  },
  cancel: {
    color: Palette.color_ff2,
    fontWeight: '500',
  },
});
export default styles;
