import {BaseScreenProps} from 'App/@types/screen-type';
import media from 'App/assets/media';
import {ScreenMap} from 'App/Config/NavigationConfig';
import {code} from 'App/Lib/codeHelpers';
import {Actions as GlobalAction} from 'App/Redux/GlobalRedux';
import {Actions as InfoAction} from 'App/Redux/InfoRedux';
import {selectUserData} from 'App/Selector/AuthSelector';
import React, {ReactElement, useEffect, useState} from 'react';
import {Image, SafeAreaView, ScrollView, Text, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import styles from './styles';

const ShopProfileScreen = (
  props: BaseScreenProps<ScreenMap.ShopProfile>,
): ReactElement => {
  const dispatch = useDispatch();
  const userInfo = useSelector(selectUserData);
  const [shopInfo, setShopInfo] = useState({
    address: '',
    name: '',
    phone1: '',
    phone2: '',
  });

  useEffect(() => {
    const unsubcribe = props.navigation.addListener('focus', () => {
      dispatch(
        InfoAction.getInfoShop(
          {},
          {
            onBeginning: () => {
              dispatch(GlobalAction.setShowLoading({isLoading: true}));
            },
            onSuccess: (response: any) => {
              switch (response.errorCode) {
                case code.success:
                  setShopInfo({
                    address: response.data.address,
                    name: response.data.name,
                    phone1: response.data.phone1,
                    phone2: response.data.phone2,
                  });
                  break;
                case code.unauthorize:
                  props.navigation.navigate(ScreenMap.SignIn, {});
                  break;
              }
            },
            onFailure: () => {},
            onFinish: () => {
              dispatch(GlobalAction.setShowLoading({isLoading: true}));
            },
          },
        ),
      );
    });
    return unsubcribe;
  }, [dispatch, props.navigation]);

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.container}>
        <View style={styles.infoHolder}>
          <View style={styles.groupInfoAvatar}>
            <View style={styles.groupAvatar}>
              <View>
                <Image source={media.avatar_default} style={styles.avatar} />
              </View>
              <View style={styles.groupUserName}>
                <Text style={styles.txtUserName}>{userInfo.fullName}</Text>
                <Text style={styles.txtUserCode}>{userInfo.userName}</Text>
              </View>
            </View>
          </View>
          <View style={styles.groupUserInfo}>
            <View style={styles.groupInfo}>
              <Text style={styles.txtLabelInfo}>{'Tên shop'}</Text>
              <Text style={styles.txtInfo}>{shopInfo.name}</Text>
            </View>
            <View style={styles.groupInfo}>
              <Text style={styles.txtLabelInfo}>{'Số điện thoại 1'}</Text>
              <Text style={styles.txtInfo}>{shopInfo.phone1}</Text>
            </View>
            <View style={styles.groupInfo}>
              <Text style={styles.txtLabelInfo}>{'Số điện thoại 2'}</Text>
              <Text style={styles.txtInfo}>{shopInfo.phone2}</Text>
            </View>
            <View style={styles.groupInfo}>
              <Text style={styles.txtLabelInfo}>{'Địa chỉ shop'}</Text>
              <Text style={styles.txtInfo}>{shopInfo.address}</Text>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
export default ShopProfileScreen;
