export const code = {
  unauthorize: -1,
  phone_invalid: -4,
  success: 1,
  phone_exist: -49,
  otp_code_wrong: -10,
  wrong_info_login: -18,
};

export const user_code = {
  block: -1,
  registered: 0,
  verified_otp: 1,
  updated_info: 2,
  accepted_tos: 3,
  updated_shop: 4,
  register_done: 5,
  shop_approve: 6,
};
