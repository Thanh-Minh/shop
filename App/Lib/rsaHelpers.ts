import env from 'App/Config/Enviroment/env';
import {RSA} from 'react-native-rsa-native';

export async function encryptString(txt: string) {
  let encryptTXT = await RSA.encrypt(txt, env.PUBLIC_KEY);
  return encryptTXT.replace(/(\r\n|\n|\r)/gm, '');
}
