import {produce} from 'immer';
import {
  createAction,
  createActionTypeOnSuccess,
  UnfoldSagaActionType,
} from 'redux-unfold-saga';

export const REDUX_KEY = 'Data';
export enum ActionTypes {
  GET_DISTRICT = 'GET_DISTRICT',
  GET_WARD = 'GET_WARD',
  GET_CATEGORIES = 'GET_CATEGORIES',
  GET_SUB_CATEGORIES = 'GET_SUB_CATEGORIES',
  UPLOAD_IMAGE = 'UPLOAD_IMAGE',
  GET_LIST_ORDER = 'GET_LIST_ORDER',
  COMPLETE_ORDER = 'COMPLETE_ORDER',
  GET_LIST_UNITS = 'GET_LIST_UNITS',
  GET_LIST_PRODUCT = 'GET_LIST_PRODUCT',
  GET_LIST_PRODUCT_CHILD = 'GET_LIST_PRODUCT_CHILD',
  GET_ALL_PRODUCT = 'GET_ALL_PRODUCT',
  GET_LIST_BANK = 'GET_LIST_BANK',
  GET_DATA_STATISTIC = 'GET_DATA_STATISTIC',
  GET_LIST_PRODUCT_STATISTIC = 'GET_LIST_PRODUCT_STATISTIC',
  GET_PRODUCT_BY_ID = 'GET_PRODUCT_BY_ID',
  GET_LIST_NOTIFICATION = 'GET_LIST_NOTIFICATION',
}

export const Actions = {
  getDistrict: createAction(ActionTypes.GET_DISTRICT),
  getWard: createAction(ActionTypes.GET_WARD),
  getCategories: createAction(ActionTypes.GET_CATEGORIES),
  getSubCategories: createAction(ActionTypes.GET_SUB_CATEGORIES),
  uploadImage: createAction(ActionTypes.UPLOAD_IMAGE),
  getListOrder: createAction(ActionTypes.GET_LIST_ORDER),
  completeOrder: createAction(ActionTypes.COMPLETE_ORDER),
  getListUnits: createAction(ActionTypes.GET_LIST_UNITS),
  getListProduct: createAction(ActionTypes.GET_LIST_PRODUCT),
  getListProductChild: createAction(ActionTypes.GET_LIST_PRODUCT_CHILD),
  getAllProduct: createAction(ActionTypes.GET_ALL_PRODUCT),
  getListBank: createAction(ActionTypes.GET_LIST_BANK),
  getDataStatistic: createAction(ActionTypes.GET_DATA_STATISTIC),
  getListProductStatistic: createAction(ActionTypes.GET_LIST_PRODUCT_STATISTIC),
  getProductById: createAction(ActionTypes.GET_PRODUCT_BY_ID),
  getListNotification: createAction(ActionTypes.GET_LIST_NOTIFICATION),
};

export interface DataState {
  data_district: {
    createdTime: string;
    id: number;
    name: string;
    updatedTime: string;
  }[];
  data_ward: {
    createdTime: number;
    districtId: number;
    id: number;
    name: string;
    updatedTime: number;
  }[];
  data_list_order: OrderItem[];
}

export const defaultState: DataState = {
  data_district: [
    {
      createdTime: '',
      id: 0,
      name: '',
      updatedTime: '',
    },
  ],
  data_ward: [
    {
      createdTime: 0,
      districtId: 0,
      id: 0,
      name: '',
      updatedTime: 0,
    },
  ],
  data_list_order: [
    {
      acceptedTime: 0,
      createdTime: 0,
      deliveryAddress: '',
      deliveryNote: '',
      detailProductId: 0,
      discount: 0,
      id: 0,
      orderCode: '',
      orderId: 0,
      packingType: '',
      productName: '',
      productPrice: 0,
      quantity: 0,
      shopId: 0,
      status: 0,
      successTime: 0,
      updatedTime: 0,
      userId: 0,
    },
  ],
};

export const reducer = (
  state = defaultState,
  action: UnfoldSagaActionType,
): DataState => {
  const {type, payload} = action;
  return produce(state, (draftState: DataState) => {
    switch (type) {
      case createActionTypeOnSuccess(ActionTypes.GET_DISTRICT):
        draftState.data_district = payload.data;
        break;
      case createActionTypeOnSuccess(ActionTypes.GET_WARD):
        draftState.data_ward = payload.data;
        break;
      case createActionTypeOnSuccess(ActionTypes.GET_LIST_ORDER):
        draftState.data_list_order = payload.data;
        break;
    }
  });
};
