import {produce} from 'immer';
import {
  createAction,
  createActionTypeOnSuccess,
  UnfoldSagaActionType,
} from 'redux-unfold-saga';

export const REDUX_KEY = 'Info';
export enum ActionTypes {
  UPDATE_OWNER_SHOP_INFO = 'UPDATE_OWNER_SHOP_INFO',
  CREATE_PRODUCT = 'CREATE_PRODUCT',
  CREATE_COMBO = 'CREATE_COMBO',
  UPDATE_PRODUCT = 'UPDATE_PRODUCT',
  UPDATE_COMBO = 'UPDATE_COMBO',
  GET_INFO_SHOP = 'GET_INFO_SHOP',
  GET_INFO_OWNER_SHOP = 'GET_INFO_OWNER_SHOP',
  SET_PRODUCT_OUTSTOCK = 'SET_PRODUCT_OUTSTOCK',
  SET_PRODUCT_QUANTITY = 'SET_PRODUCT_QUANTITY',
  UPDATE_USER_BANK_INFO = 'UPDATE_USER_BANK_INFO',
  GET_USER_BANK_INFO = 'GET_USER_BANK_INFO',
}

export const Actions = {
  updateOwnerShopInfo: createAction(ActionTypes.UPDATE_OWNER_SHOP_INFO),
  createProduct: createAction(ActionTypes.CREATE_PRODUCT),
  createCombo: createAction(ActionTypes.CREATE_COMBO),
  updateProduct: createAction(ActionTypes.UPDATE_PRODUCT),
  updateCombo: createAction(ActionTypes.UPDATE_COMBO),
  getInfoShop: createAction(ActionTypes.GET_INFO_SHOP),
  getInfoOwnerShop: createAction(ActionTypes.GET_INFO_OWNER_SHOP),
  setProductOutStock: createAction(ActionTypes.SET_PRODUCT_OUTSTOCK),
  setProductQuantity: createAction(ActionTypes.SET_PRODUCT_QUANTITY),
  updateUserBankInfo: createAction(ActionTypes.UPDATE_USER_BANK_INFO),
  getUserBankInfo: createAction(ActionTypes.GET_USER_BANK_INFO),
};

export interface InfoState {
  data_update_ownershop: UpdateOwnerInfo;
  data_info_shop: InfoShop;
  data_user_bank: UserBank;
  data_ownershop: OwnerInfo;
}

export const defaultState: InfoState = {
  data_update_ownershop: {
    address: '',
    birthday: 0,
    cert_images: [''],
    full_name: '',
    gender: 0,
    ward_id: 0,
  },
  data_info_shop: {
    address: '',
    districtId: 0,
    name: '',
    phone1: '',
    phone2: '',
    wardId: 0,
  },
  data_user_bank: {
    accName: '',
    accNum: '',
    bankCode: '',
    bankName: '',
    createdTime: 0,
    id: 0,
    updatedTime: 0,
    userId: 0,
  },
  data_ownershop: {
    address: {
      buildingId: 0,
      createdTime: 0,
      districtId: 0,
      id: 0,
      latitude: 0,
      longitude: 0,
      name: '',
      updatedTime: 0,
      userId: 0,
      wardId: 0,
    },
    birthday: 0,
    fullName: '',
    gender: 0,
    phone: '',
  },
};

export const reducer = (
  state = defaultState,
  action: UnfoldSagaActionType,
): InfoState => {
  const {type, payload} = action;
  return produce(state, (draftState: InfoState) => {
    switch (type) {
      case createActionTypeOnSuccess(ActionTypes.UPDATE_OWNER_SHOP_INFO):
        draftState.data_update_ownershop = payload.data;
        break;
      case createActionTypeOnSuccess(ActionTypes.GET_INFO_SHOP):
        draftState.data_info_shop = payload.data;
        break;
      case createActionTypeOnSuccess(ActionTypes.GET_INFO_OWNER_SHOP):
        draftState.data_ownershop = payload.data;
        break;
      case createActionTypeOnSuccess(ActionTypes.UPDATE_USER_BANK_INFO):
        draftState.data_user_bank = payload.data;
        break;
      case createActionTypeOnSuccess(ActionTypes.GET_USER_BANK_INFO):
        draftState.data_user_bank = payload.data;
        break;
    }
  });
};
