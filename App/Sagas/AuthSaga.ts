import {apiMap} from 'App/Lib/apiMap';
import {appApi} from 'App/Lib/fetchHelpers';
import {encryptString} from 'App/Lib/rsaHelpers';
import {ActionTypes} from 'App/Redux/AuthRedux';
import {Platform} from 'react-native';
import GetLocation from 'react-native-get-location';
import {SagaIterator} from 'redux-saga';
import {takeLatest} from 'redux-saga/effects';
import {unfoldSaga, UnfoldSagaActionType} from 'redux-unfold-saga';

export function* takeLogin({
  payload: {phone, password},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        let encryptPass = await encryptString(password);
        const {data} = await appApi.post(apiMap.login, {
          device_token: '',
          ip: '',
          password: encryptPass,
          source: Platform.OS.toUpperCase,
          topicName: '',
          type: 1,
          username: phone,
        });
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeSendRegisterData({
  payload: {phone, password},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        let encryptPass = await encryptString(password);
        const {data} = await appApi.post(apiMap.sendRegisterData, {
          password: encryptPass,
          type: 1,
          username: phone,
        });
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeVerifyPhone({
  payload: {otp},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.post(apiMap.verifyPhone, {
          otp: otp,
        });
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeAcceptTermOfService({
  payload: {},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.post(apiMap.acceptTermOfService, {});
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeUpdateShopInfo({
  payload: {
    shopName,
    phone,
    categories,
    listImage,
    afternoon,
    morning,
    noon,
    night,
  },
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        let lat = 0;
        let long = 0;
        GetLocation.getCurrentPosition({
          enableHighAccuracy: true,
          timeout: 15000,
        })
          .then(location => {
            lat = location.latitude;
            long = location.longitude;
          })
          .catch(error => {
            const {code, message} = error;
            console.warn(code, message);
          });
        let data_request: OwnerDataRequest = {
          cert_images: listImage,
          goods_type: categories,
          lat: lat,
          lng: long,
          name: shopName,
          phone: phone,
          saleTime: {},
        };
        afternoon ? (data_request.saleTime.afternoons = afternoon) : null;
        morning ? (data_request.saleTime.mornings = morning) : null;
        noon ? (data_request.saleTime.noons = noon) : null;
        night ? (data_request.saleTime.nights = night) : null;
        const {data} = await appApi.post(apiMap.updateShopInfo, data_request);
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeResetAuth({
  payload: {},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        return true;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeInsertDeviceFireBase({
  payload: {deviceId},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const type = Platform.OS.toUpperCase();
        const {data} = await appApi.post(apiMap.insertDeviceFireBase, {
          deviceId: deviceId,
          type: type,
        });
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeSendOtp({
  payload: {typeSend},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.post(apiMap.sendOtp, {
          type: typeSend,
        });
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export default function*(): SagaIterator {
  yield takeLatest(ActionTypes.LOGIN, takeLogin);
  yield takeLatest(ActionTypes.SEND_REGISTER_DATA, takeSendRegisterData);
  yield takeLatest(ActionTypes.VERIFY_PHONE, takeVerifyPhone);
  yield takeLatest(ActionTypes.ACCEPT_TERM_OF_SERVICE, takeAcceptTermOfService);
  yield takeLatest(ActionTypes.UPDATE_SHOP_INFO, takeUpdateShopInfo);
  yield takeLatest(ActionTypes.RESET_AUTH, takeResetAuth);
  yield takeLatest(
    ActionTypes.INSERT_DEVICE_FIRE_BASE,
    takeInsertDeviceFireBase,
  );
  yield takeLatest(ActionTypes.SEND_OTP, takeSendOtp);
}
