import {apiMap} from 'App/Lib/apiMap';
import {appApi} from 'App/Lib/fetchHelpers';
import {ActionTypes} from 'App/Redux/DataRedux';
import {SagaIterator} from 'redux-saga';
import {takeLatest} from 'redux-saga/effects';
import {unfoldSaga, UnfoldSagaActionType} from 'redux-unfold-saga';

export function* takeGetListDistrict({
  payload: {},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.get(apiMap.listDistrict);
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeGetListWard({
  payload: {district_id},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.get(
          apiMap.listWard + `?district_id=${district_id}`,
        );
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeGetCategories({
  payload: {},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.get(apiMap.listCategories);
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeGetSubCategories({
  payload: {categoryId},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.get(
          apiMap.listCategories + `/${categoryId}`,
        );
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeUploadImage({
  payload: {formData},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.post(apiMap.uploadImage, formData, {
          headers: {
            Authorization: 'SYSTEM_IMAGE',
            'Content-Type': 'multipart/form-data',
          },
        });
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeGetListOrder({
  payload: {status},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.get(apiMap.listOrder + `?status=${status}`);
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeCompleteOrder({
  payload: {cartId},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.put(
          apiMap.confirmOrder + `?cartId=${cartId}`,
        );
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeGetListUnits({
  payload: {categoryId},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.get(
          apiMap.listUnit + `?categoryId=${categoryId}`,
        );
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeGetAllProduct({
  payload: {},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.get(apiMap.listProduct);
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeGetListProduct({
  payload: {isOnSale},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.get(
          apiMap.listProductChild + `?isOnSale=${isOnSale}`,
        );
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeGetListBank({
  payload: {},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.get(apiMap.getListBank);
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeGetDataStatistic({
  payload: {startTime, endTime},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.get(
          apiMap.getDataStatistic +
            `?startTime=${startTime}&endTime=${endTime}`,
        );
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeGetListProductStatistic({
  payload: {},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.get(apiMap.getListProductStatistic);
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeGetProductById({
  payload: {productId},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.get(apiMap.getProductById + `${productId}`);
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeGetListNotification({
  payload: {deviceId, typeNoti, endTime, page, pageSize, startTime},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        let sendData: FilterNoti = {
          data: {
            deviceId: deviceId,
            type: typeNoti,
          },
          page: page,
          pageSize: pageSize,
        };
        endTime ? (sendData.endTime = endTime) : null;
        startTime ? (sendData.startTime = startTime) : null;
        const {data} = await appApi.post(apiMap.getListNoti, sendData);
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export default function*(): SagaIterator {
  yield takeLatest(ActionTypes.GET_DISTRICT, takeGetListDistrict);
  yield takeLatest(ActionTypes.GET_WARD, takeGetListWard);
  yield takeLatest(ActionTypes.GET_CATEGORIES, takeGetCategories);
  yield takeLatest(ActionTypes.GET_SUB_CATEGORIES, takeGetSubCategories);
  yield takeLatest(ActionTypes.UPLOAD_IMAGE, takeUploadImage);
  yield takeLatest(ActionTypes.GET_LIST_ORDER, takeGetListOrder);
  yield takeLatest(ActionTypes.GET_LIST_UNITS, takeGetListUnits);
  yield takeLatest(ActionTypes.COMPLETE_ORDER, takeCompleteOrder);
  yield takeLatest(ActionTypes.GET_LIST_PRODUCT, takeGetListProduct);
  yield takeLatest(ActionTypes.GET_ALL_PRODUCT, takeGetAllProduct);
  yield takeLatest(ActionTypes.GET_LIST_BANK, takeGetListBank);
  yield takeLatest(
    ActionTypes.GET_LIST_PRODUCT_STATISTIC,
    takeGetListProductStatistic,
  );
  yield takeLatest(ActionTypes.GET_DATA_STATISTIC, takeGetDataStatistic);
  yield takeLatest(ActionTypes.GET_PRODUCT_BY_ID, takeGetProductById);
  yield takeLatest(ActionTypes.GET_LIST_NOTIFICATION, takeGetListNotification);
}
