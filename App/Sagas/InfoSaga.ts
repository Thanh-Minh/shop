import {apiMap} from 'App/Lib/apiMap';
import {appApi} from 'App/Lib/fetchHelpers';
import {ActionTypes} from 'App/Redux/InfoRedux';
import {SagaIterator} from 'redux-saga';
import {takeLatest} from 'redux-saga/effects';
import {unfoldSaga, UnfoldSagaActionType} from 'redux-unfold-saga';

export function* takeUpdateOwnerInfo({
  payload: {address, birthday, full_name, gender, ward_id, listImage},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.post(apiMap.updateOwnerShopInfo, {
          address: address,
          birthday: birthday,
          full_name: full_name,
          cert_images: listImage,
          gender: gender,
          ward_id: ward_id,
        });
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeCreateProduct({
  payload: {
    image,
    name,
    note,
    groupId,
    classId,
    categoryId,
    unitId,
    basePrice,
    discount,
    companyDiscount,
    expiredDate,
    description,
    dataProductPrice,
  },
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.post(apiMap.createProduct, {
          basePrice: basePrice,
          categoryId: categoryId,
          classId: classId,
          companyDiscount: companyDiscount,
          description: description,
          discount: discount,
          expiredTime: expiredDate,
          groupId: groupId,
          images: [image],
          listDetailProdMongo: dataProductPrice,
          name: name,
          note: note,
          special: 0,
          type: 0,
          unitId: unitId,
        });
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeUpdateProduct({
  payload: {
    id,
    productId,
    image,
    name,
    note,
    groupId,
    classId,
    categoryId,
    unitId,
    basePrice,
    discount,
    companyDiscount,
    expiredDate,
    description,
    dataProductPrice,
  },
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.post(apiMap.createProduct, {
          id: id,
          basePrice: basePrice,
          categoryId: categoryId,
          classId: classId,
          companyDiscount: companyDiscount,
          description: description,
          discount: discount,
          expiredTime: expiredDate,
          groupId: groupId,
          images: [image],
          listDetailProdMongo: dataProductPrice,
          name: name,
          note: note,
          productId: productId,
          special: 0,
          type: 0,
          unitId: unitId,
        });
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeCreateCombo({
  payload: {
    image,
    name,
    note,
    discount,
    companyDiscount,
    expiredDate,
    description,
    dataProductPrice,
    dataProduct,
  },
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.post(apiMap.createProduct, {
          companyDiscount: companyDiscount,
          description: description,
          discount: discount,
          expiredTime: expiredDate,
          images: [image],
          listComboProduct: dataProduct,
          listDetailProdMongo: dataProductPrice,
          name: name,
          note: note,
          special: 0,
          type: 1,
        });
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeUpdateCombo({
  payload: {
    id,
    productId,
    image,
    name,
    note,
    discount,
    companyDiscount,
    expiredDate,
    description,
    dataProductPrice,
    dataProduct,
  },
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.post(apiMap.createProduct, {
          id: id,
          companyDiscount: companyDiscount,
          description: description,
          discount: discount,
          expiredTime: expiredDate,
          images: [image],
          listComboProduct: dataProduct,
          listDetailProdMongo: dataProductPrice,
          name: name,
          note: note,
          productId: productId,
          special: 0,
          type: 1,
        });
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeGetInfoShop({
  payload: {},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.get(apiMap.getShopInfo);
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeGetInfoOwnerShop({
  payload: {},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.get(apiMap.getOwnerShopInfo);
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeSetProductOutStock({
  payload: {productId},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.put(apiMap.setOutStock, {
          detailProductId: productId,
          quantity: 0,
        });
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeSetProductQuantity({
  payload: {productId, quantity},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.put(apiMap.setOutStock, {
          detailProductId: productId,
          quantity: quantity,
        });
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeUpdateUserInfoBank({
  payload: {bankCode, bankNumber, fullName, otp},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.put(apiMap.updateUserBankInfo, {
          bank_code: bankCode,
          bank_number: bankNumber,
          full_name: fullName,
          otp: otp,
        });
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export function* takeUserInfoBank({
  payload: {},
  callbacks,
  type,
}: UnfoldSagaActionType): Iterable<SagaIterator> {
  yield unfoldSaga(
    {
      handler: async (): Promise<{}> => {
        const {data} = await appApi.get(apiMap.updateUserBankInfo);
        return data;
      },
      key: type,
    },
    callbacks,
  );
}

export default function*(): SagaIterator {
  yield takeLatest(ActionTypes.UPDATE_OWNER_SHOP_INFO, takeUpdateOwnerInfo);
  yield takeLatest(ActionTypes.CREATE_PRODUCT, takeCreateProduct);
  yield takeLatest(ActionTypes.CREATE_COMBO, takeCreateCombo);
  yield takeLatest(ActionTypes.UPDATE_PRODUCT, takeUpdateProduct);
  yield takeLatest(ActionTypes.UPDATE_COMBO, takeUpdateCombo);
  yield takeLatest(ActionTypes.GET_INFO_SHOP, takeGetInfoShop);
  yield takeLatest(ActionTypes.GET_INFO_OWNER_SHOP, takeGetInfoOwnerShop);
  yield takeLatest(ActionTypes.SET_PRODUCT_OUTSTOCK, takeSetProductOutStock);
  yield takeLatest(ActionTypes.SET_PRODUCT_QUANTITY, takeSetProductQuantity);
  yield takeLatest(ActionTypes.UPDATE_USER_BANK_INFO, takeUpdateUserInfoBank);
  yield takeLatest(ActionTypes.GET_USER_BANK_INFO, takeUserInfoBank);
}
