import {SagaIterator} from 'redux-saga';
import {all, call} from 'redux-saga/effects';
import auth from './AuthSaga';
import data from './DataSaga';
import info from './InfoSaga';

export default function* rootSaga(): SagaIterator {
  yield all([call(auth), call(info), call(data)]);
}
