import { RootState } from 'App/Redux';
import { AuthState, REDUX_KEY } from 'App/Redux/AuthRedux';
import { get } from 'lodash';
import { createSelector } from 'reselect';

export const selectAuth = (state: RootState): AuthState =>
  get(state, REDUX_KEY);

export const selectAuthData = createSelector(
  selectAuth,
  (authState: AuthState): AuthInfo => authState.data_auth,
);

export const selectUserData = createSelector(
  selectAuth,
  (authState: AuthState): UserInfo => authState.user_info,
);

export default {
  selectAuthData,
  selectUserData,
};
