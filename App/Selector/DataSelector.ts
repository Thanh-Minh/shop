import {RootState} from 'App/Redux';
import {DataState, REDUX_KEY} from 'App/Redux/DataRedux';
import {get} from 'lodash';

export const selectData = (state: RootState): DataState =>
  get(state, REDUX_KEY);

// export const selectListOrder = createSelector(
//   selectData,
//   (dataState: DataState): OrderItem[] => dataState.data_list_order,
// );

// export default {
//   selectListOrder,
// };
