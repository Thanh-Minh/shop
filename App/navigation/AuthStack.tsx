import {createStackNavigator} from '@react-navigation/stack';
import ImageButtonBack from 'App/Components/ImageButtonBack';
import ImageHeader from 'App/Components/ImageHeader';
import {ScreenMap, ScreenParams} from 'App/Config/NavigationConfig';
import CheckLoginScreen from 'App/Containers/Auth/CheckLoginScreen';
import OtpScreen from 'App/Containers/Auth/OtpScreen';
import ProvisionScreen from 'App/Containers/Auth/ProvisionScreen';
import SignInScreen from 'App/Containers/Auth/SignInScreen';
import SignUpScreen from 'App/Containers/Auth/SignUpScreen';
import UpdateOwnerShopInfoScreen from 'App/Containers/Auth/UpdateOwnerShopInfoScreen';
import UpdateShopInfoScreen from 'App/Containers/Auth/UpdateShopInfoScreen';
import {Palette} from 'App/Theme/Palette';
import React from 'react';
import {RFValue} from 'react-native-responsive-fontsize';

const Stack = createStackNavigator<ScreenParams>();

const AuthStack = () => {
  return (
    <Stack.Navigator initialRouteName={ScreenMap.CheckLogin}>
      <Stack.Screen
        name={ScreenMap.CheckLogin}
        component={CheckLoginScreen}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name={ScreenMap.SignIn}
        component={SignInScreen}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name={ScreenMap.SignUp}
        component={SignUpScreen}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name={ScreenMap.Otp}
        component={OtpScreen}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name={ScreenMap.Provision}
        component={ProvisionScreen}
        options={{
          headerTitle: 'Điều khoản sử dụng - Kênh người bán',
          headerLeft: () => {
            return null;
          },
          headerBackground: () => <ImageHeader />,
          headerTintColor: Palette.white,
          headerTitleStyle: {fontSize: RFValue(15, 580)},
          headerTitleAlign: 'center',
        }}
      />
      <Stack.Screen
        name={ScreenMap.UpdateOwnerShopInfo}
        component={UpdateOwnerShopInfoScreen}
        options={{
          headerTitle: 'Cập nhật thông tin chủ shop',
          headerLeft: () => {
            return null;
          },
          headerBackground: () => <ImageHeader />,
          headerTintColor: Palette.white,
          headerTitleStyle: {fontSize: RFValue(15, 580)},
          headerTitleAlign: 'center',
        }}
      />
      <Stack.Screen
        name={ScreenMap.UpdateShopInfo}
        component={UpdateShopInfoScreen}
        options={{
          headerTitle: 'Cập nhật thông tin shop',
          headerBackTitleVisible: false,
          headerLeft: () => <ImageButtonBack />,
          headerBackground: () => <ImageHeader />,
          headerTintColor: Palette.white,
          headerTitleStyle: {fontSize: RFValue(15, 580)},
          headerTitleAlign: 'center',
        }}
      />
    </Stack.Navigator>
  );
};
export default AuthStack;
