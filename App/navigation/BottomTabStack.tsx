import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import media from 'App/assets/media';
import {ScreenMap} from 'App/Config/NavigationConfig';
import {Palette} from 'App/Theme/Palette';
import React from 'react';
import {Image} from 'react-native';
import {getBottomSpace} from 'react-native-iphone-x-helper';
import {RFValue} from 'react-native-responsive-fontsize';
import HomeStack from './HomeStack';
import NotificationStack from './NotificationStack';
import OrderStack from './OrderStack';
import StatisticStack from './StatisticStack';
import UserStack from './UserStack';

const Tab = createBottomTabNavigator();

const BottomTabStack = () => {
  const getTabBarVisibility = (route: any) => {
    const routeName = route.state
      ? route.state.routes[route.state.index].name
      : '';

    if (
      routeName === ScreenMap.CreateProduct ||
      routeName === ScreenMap.CreateCombo ||
      routeName === ScreenMap.UpdateProduct ||
      routeName === ScreenMap.UpdateCombo ||
      routeName === ScreenMap.OrderDetail ||
      routeName === ScreenMap.NotificationDetail ||
      routeName === ScreenMap.Profile ||
      routeName === ScreenMap.ShopProfile ||
      routeName === ScreenMap.BankProfile
    ) {
      return false;
    }

    return true;
  };

  return (
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: Palette.color_e91,
        labelStyle: {
          fontSize: RFValue(9, 580),
        },
        style: {
          height: 50,
          paddingBottom: 0,
          marginBottom: getBottomSpace(),
        },
      }}>
      <Tab.Screen
        name="Home"
        component={HomeStack}
        options={({route}) => ({
          tabBarLabel: 'Sản Phẩm',
          tabBarIcon: ({focused}) => (
            <Image
              source={focused ? media.product : media.product_inactive}
              style={{width: 25, height: 25, resizeMode: 'contain'}}
            />
          ),
          tabBarVisible: getTabBarVisibility(route),
        })}
      />
      <Tab.Screen
        name="Order"
        component={OrderStack}
        options={({route}) => ({
          tabBarLabel: 'Đơn hàng',
          tabBarIcon: ({focused}) => (
            <Image
              source={focused ? media.list : media.list_inactive}
              style={{width: 25, height: 25, resizeMode: 'contain'}}
            />
          ),
          tabBarVisible: getTabBarVisibility(route),
        })}
      />
      <Tab.Screen
        name="Statistic"
        component={StatisticStack}
        options={({route}) => ({
          tabBarLabel: 'Thống kê',
          tabBarIcon: ({focused}) => (
            <Image
              source={focused ? media.statistic : media.statistic_inactive}
              style={{width: 25, height: 25, resizeMode: 'contain'}}
            />
          ),
          tabBarVisible: getTabBarVisibility(route),
        })}
      />
      <Tab.Screen
        name="Notify"
        component={NotificationStack}
        options={({route}) => ({
          tabBarLabel: 'Thông báo',
          tabBarIcon: ({focused}) => (
            <Image
              source={focused ? media.notify : media.notify_inactive}
              style={{width: 25, height: 25, resizeMode: 'contain'}}
            />
          ),
          tabBarVisible: getTabBarVisibility(route),
        })}
      />
      <Tab.Screen
        name="User"
        component={UserStack}
        options={({route}) => ({
          tabBarLabel: 'Tài khoản',
          tabBarIcon: ({focused}) => (
            <Image
              source={
                focused ? media.user_bottom_tab : media.user_bottom_tab_inactive
              }
              style={{width: 25, height: 25, resizeMode: 'contain'}}
            />
          ),
          tabBarVisible: getTabBarVisibility(route),
        })}
      />
    </Tab.Navigator>
  );
};
export default BottomTabStack;
