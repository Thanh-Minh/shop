import {useNavigation} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import ImageButtonBack from 'App/Components/ImageButtonBack';
import ImageHeader from 'App/Components/ImageHeader';
import {ScreenMap, ScreenParams} from 'App/Config/NavigationConfig';
import CreateComboScreen from 'App/Containers/Main/CreateComboScreen';
import CreateProductScreen from 'App/Containers/Main/CreateProductScreen';
import HomeScreen from 'App/Containers/Main/HomeScreen';
import UpdateComboScreen from 'App/Containers/Main/UpdateComboScreen';
import UpdateProductScreen from 'App/Containers/Main/UpdateProductScreen';
import {Palette} from 'App/Theme/Palette';
import React from 'react';
import {RFValue} from 'react-native-responsive-fontsize';

const Stack = createStackNavigator<ScreenParams>();

const HomeStack = () => {
  const navigation = useNavigation();
  return (
    <Stack.Navigator initialRouteName={ScreenMap.Home}>
      <Stack.Screen
        name={ScreenMap.Home}
        component={HomeScreen}
        options={{
          headerTitle: 'Sản phẩm',
          headerLeft: () => {
            return null;
          },
          headerBackground: () => <ImageHeader />,
          headerTintColor: Palette.white,
          headerTitleStyle: {fontSize: RFValue(15, 580)},
          headerTitleAlign: 'center',
        }}
      />
      <Stack.Screen
        name={ScreenMap.CreateProduct}
        component={CreateProductScreen}
        options={{
          headerTitle: 'Tạo sản phẩm',
          headerBackTitleVisible: false,
          headerLeft: () => <ImageButtonBack />,
          headerBackground: () => <ImageHeader />,
          headerTintColor: Palette.white,
          headerTitleStyle: {fontSize: RFValue(15, 580)},
          headerTitleAlign: 'center',
        }}
      />
      <Stack.Screen
        name={ScreenMap.CreateCombo}
        component={CreateComboScreen}
        options={{
          headerTitle: 'Tạo nhóm sản phẩm',
          headerBackTitleVisible: false,
          headerLeft: () => <ImageButtonBack />,
          headerBackground: () => <ImageHeader />,
          headerTintColor: Palette.white,
          headerTitleStyle: {fontSize: RFValue(15, 580)},
          headerTitleAlign: 'center',
        }}
      />
      <Stack.Screen
        name={ScreenMap.UpdateProduct}
        component={UpdateProductScreen}
        options={{
          headerTitle: 'Cập nhật sản phẩm',
          headerBackTitleVisible: false,
          headerLeft: () => <ImageButtonBack />,
          headerBackground: () => <ImageHeader />,
          headerTintColor: Palette.white,
          headerTitleStyle: {fontSize: RFValue(15, 580)},
          headerTitleAlign: 'center',
        }}
      />
      <Stack.Screen
        name={ScreenMap.UpdateCombo}
        component={UpdateComboScreen}
        options={{
          headerTitle: 'Cập nhật nhóm sản phẩm',
          headerBackTitleVisible: false,
          headerLeft: () => <ImageButtonBack />,
          headerBackground: () => <ImageHeader />,
          headerTintColor: Palette.white,
          headerTitleStyle: {fontSize: RFValue(15, 580)},
          headerTitleAlign: 'center',
        }}
      />
    </Stack.Navigator>
  );
};
export default HomeStack;
