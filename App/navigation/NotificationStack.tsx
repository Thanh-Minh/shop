import {createStackNavigator} from '@react-navigation/stack';
import media from 'App/assets/media';
import ImageButtonBack from 'App/Components/ImageButtonBack';
import ImageHeader from 'App/Components/ImageHeader';
import {ScreenMap, ScreenParams} from 'App/Config/NavigationConfig';
import NotificationDetailScreen from 'App/Containers/Notify/NotificationDetailScreen';
import NotificationScreen from 'App/Containers/Notify/NotificationScreen';
import {Palette} from 'App/Theme/Palette';
import React from 'react';
import {Image, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

const Stack = createStackNavigator<ScreenParams>();

const NotificationStack = () => {
  return (
    <Stack.Navigator initialRouteName={ScreenMap.Notification}>
      <Stack.Screen
        name={ScreenMap.Notification}
        component={NotificationScreen}
        options={{
          headerTitle: 'Thông báo',
          headerLeft: () => {
            return null;
          },
          headerRight: () => {
            return (
              <TouchableOpacity onPress={() => {}}>
                <Image
                  source={media.notify_check_all}
                  style={{
                    width: 20,
                    height: 20,
                    marginRight: 15,
                    resizeMode: 'contain',
                  }}
                />
              </TouchableOpacity>
            );
          },
          headerBackground: () => <ImageHeader />,
          headerTintColor: Palette.white,
          headerTitleStyle: {fontSize: RFValue(15, 580)},
          headerTitleAlign: 'center',
        }}
      />
      <Stack.Screen
        name={ScreenMap.NotificationDetail}
        component={NotificationDetailScreen}
        options={{
          headerTitle: 'Thông báo',
          headerBackTitleVisible: false,
          headerLeft: () => <ImageButtonBack />,
          headerRight: () => {
            return (
              <TouchableOpacity onPress={() => {}}>
                <Image
                  source={media.notify_check_all}
                  style={{
                    width: 25,
                    height: 25,
                    marginRight: 15,
                    resizeMode: 'contain',
                  }}
                />
              </TouchableOpacity>
            );
          },
          headerBackground: () => <ImageHeader />,
          headerTintColor: Palette.white,
          headerTitleStyle: {fontSize: RFValue(15, 580)},
          headerTitleAlign: 'center',
        }}
      />
    </Stack.Navigator>
  );
};
export default NotificationStack;
