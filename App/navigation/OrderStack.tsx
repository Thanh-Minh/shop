import {createStackNavigator} from '@react-navigation/stack';
import ImageButtonBack from 'App/Components/ImageButtonBack';
import ImageHeader from 'App/Components/ImageHeader';
import {ScreenMap, ScreenParams} from 'App/Config/NavigationConfig';
import ListOrderScreen from 'App/Containers/Order/ListOrderScreen';
import OrderDetailScreen from 'App/Containers/Order/OrderDetailScreen';
import {Palette} from 'App/Theme/Palette';
import React from 'react';
import {RFValue} from 'react-native-responsive-fontsize';

const Stack = createStackNavigator<ScreenParams>();

const OrderStack = () => {
  return (
    <Stack.Navigator initialRouteName={ScreenMap.ListOrder}>
      <Stack.Screen
        name={ScreenMap.ListOrder}
        component={ListOrderScreen}
        options={{
          headerTitle: 'Đơn hàng',
          headerLeft: () => {
            return null;
          },
          headerBackground: () => <ImageHeader />,
          headerTintColor: Palette.white,
          headerTitleStyle: {fontSize: RFValue(15, 580)},
          headerTitleAlign: 'center',
        }}
      />
      <Stack.Screen
        name={ScreenMap.OrderDetail}
        component={OrderDetailScreen}
        options={{
          headerTitle: 'Chi tiết đơn hàng',
          headerBackTitleVisible: false,
          headerLeft: () => <ImageButtonBack />,
          headerBackground: () => <ImageHeader />,
          headerTintColor: Palette.white,
          headerTitleStyle: {fontSize: RFValue(15, 580)},
          headerTitleAlign: 'center',
        }}
      />
    </Stack.Navigator>
  );
};
export default OrderStack;
