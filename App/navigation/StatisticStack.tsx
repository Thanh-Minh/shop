import {createStackNavigator} from '@react-navigation/stack';
import ImageHeader from 'App/Components/ImageHeader';
import {ScreenMap, ScreenParams} from 'App/Config/NavigationConfig';
import StatisticScreen from 'App/Containers/StatisticScreen';
import {Palette} from 'App/Theme/Palette';
import React from 'react';
import {RFValue} from 'react-native-responsive-fontsize';

const Stack = createStackNavigator<ScreenParams>();

const StatisticStack = () => {
  return (
    <Stack.Navigator initialRouteName={ScreenMap.Statistic}>
      <Stack.Screen
        name={ScreenMap.Statistic}
        component={StatisticScreen}
        options={{
          headerTitle: 'Thống kê',
          headerLeft: () => {
            return null;
          },
          headerBackground: () => <ImageHeader />,
          headerTintColor: Palette.white,
          headerTitleStyle: {fontSize: RFValue(15, 580)},
          headerTitleAlign: 'center',
        }}
      />
    </Stack.Navigator>
  );
};
export default StatisticStack;
