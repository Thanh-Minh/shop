import {createStackNavigator} from '@react-navigation/stack';
import ImageButtonBack from 'App/Components/ImageButtonBack';
import ImageHeader from 'App/Components/ImageHeader';
import {ScreenMap, ScreenParams} from 'App/Config/NavigationConfig';
import BankProfileScreen from 'App/Containers/User/BankProfileScreen';
import MainProfileScreen from 'App/Containers/User/MainProfileScreen';
import ProfileScreen from 'App/Containers/User/ProfileScreen';
import ShopProfileScreen from 'App/Containers/User/ShopProfileScreen';
import {Palette} from 'App/Theme/Palette';
import React from 'react';
import {RFValue} from 'react-native-responsive-fontsize';

const Stack = createStackNavigator<ScreenParams>();

const UserStack = () => {
  return (
    <Stack.Navigator initialRouteName={ScreenMap.MainProfile}>
      <Stack.Screen
        name={ScreenMap.MainProfile}
        component={MainProfileScreen}
        options={{
          headerTitle: 'Tài khoản',
          headerLeft: () => {
            return null;
          },
          headerBackground: () => <ImageHeader />,
          headerTintColor: Palette.white,
          headerTitleStyle: {fontSize: RFValue(15, 580)},
          headerTitleAlign: 'center',
        }}
      />
      <Stack.Screen
        name={ScreenMap.Profile}
        component={ProfileScreen}
        options={{
          headerTitle: 'Thông tin tài khoản',
          headerBackTitleVisible: false,
          headerLeft: () => <ImageButtonBack />,
          headerBackground: () => <ImageHeader />,
          headerTintColor: Palette.white,
          headerTitleStyle: {fontSize: RFValue(15, 580)},
          headerTitleAlign: 'center',
        }}
      />
      <Stack.Screen
        name={ScreenMap.ShopProfile}
        component={ShopProfileScreen}
        options={{
          headerTitle: 'Thông tin cửa hàng',
          headerBackTitleVisible: false,
          headerLeft: () => <ImageButtonBack />,
          headerBackground: () => <ImageHeader />,
          headerTintColor: Palette.white,
          headerTitleStyle: {fontSize: RFValue(15, 580)},
          headerTitleAlign: 'center',
        }}
      />
      <Stack.Screen
        name={ScreenMap.BankProfile}
        component={BankProfileScreen}
        options={{
          headerTitle: 'Thông tin ngân hàng',
          headerBackTitleVisible: false,
          headerLeft: () => <ImageButtonBack />,
          headerBackground: () => <ImageHeader />,
          headerTintColor: Palette.white,
          headerTitleStyle: {fontSize: RFValue(15, 580)},
          headerTitleAlign: 'center',
        }}
      />
    </Stack.Navigator>
  );
};
export default UserStack;
