import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {ScreenParams} from 'App/Config/NavigationConfig';
import React from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import AuthStack from './AuthStack';
import BottomTabStack from './BottomTabStack';

const Stack = createStackNavigator<ScreenParams>();

const Navigation = () => {
  return (
    <SafeAreaProvider>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Auth">
          <Stack.Screen
            name="Auth"
            component={AuthStack}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="BottomTab"
            component={BottomTabStack}
            options={{headerShown: false}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  );
};
export default Navigation;
